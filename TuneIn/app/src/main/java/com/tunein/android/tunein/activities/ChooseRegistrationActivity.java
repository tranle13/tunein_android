package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.RegisterChoiceFragment;
import com.tunein.android.tunein.interfaces.ChooseRegistration;

public class ChooseRegistrationActivity extends AppCompatActivity implements ChooseRegistration {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_registration);

		getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer_2, new RegisterChoiceFragment()).commit();
	}

	@Override
	public void chooseRegistration(Boolean isDJ) {
		Intent newAct = new Intent(this, RegistrationActivity.class);
		newAct.putExtra("isDJ", isDJ);
		startActivity(newAct);
	}
}
