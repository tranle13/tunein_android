package com.tunein.android.tunein.adapters;

import android.content.Context;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.custom_objects.Track;

import java.util.Objects;

public class RequestedSongAdapter extends BaseAdapter {

	// Member variables
	private ArrayMap<String, String> requestedSong;
	private Context mContext;
	private static final int BASE_ID = 0x1010;
	private DatabaseReference mRef;
	private FirebaseAuth mAuth;

	// Initializer
	public RequestedSongAdapter(Context _context, ArrayMap<String, String> songs) {
		mContext = _context;
		requestedSong = songs;
		mRef = FirebaseDatabase.getInstance().getReference();
		mAuth = FirebaseAuth.getInstance();
	}

	@Override
	public int getCount() {
		if (requestedSong != null) {
			return requestedSong.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if (requestedSong != null && 0 <= position || position < Objects.requireNonNull(requestedSong).size()) {
			return requestedSong.get(requestedSong.keyAt(position));
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return BASE_ID + position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder vh;

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_requesttodj_listrow, parent,
					false);
			vh = new ViewHolder(convertView);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder)convertView.getTag();
		}

		final String uid = requestedSong.get(requestedSong.keyAt(position));
		if (uid != null) {
			vh.textView_Title.setText(uid);

			vh.button_Refuse.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mAuth.getUid() != null) {
						mRef.child("dj_users").child(mAuth.getUid()).child("requested_songs").child(requestedSong.keyAt(position)).removeValue();
					}
				}
			});

			vh.button_Accept.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mAuth.getUid() != null) {
						mRef.child("dj_users").child(mAuth.getUid()).child("approved_songs").child(requestedSong.keyAt(position)).setValue(uid);
						mRef.child("dj_users").child(mAuth.getUid()).child("requested_songs").child(requestedSong.keyAt(position)).removeValue();
					}
				}
			});
		}

		return convertView;
	}

	// Recycle view pattern
	static class ViewHolder {
		final TextView textView_Title;
		final ImageButton button_Accept;
		final ImageButton button_Refuse;

		ViewHolder(View layout) {
			textView_Title = layout.findViewById(R.id.textView_Title);
			button_Accept = layout.findViewById(R.id.button_Accept);
			button_Refuse = layout.findViewById(R.id.button_Refuse);
		}
	}
}
