package com.tunein.android.tunein.fragments;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.tunein.android.tunein.R;

public class DJQrCodeFragment extends Fragment {

	private FirebaseAuth mAuth;

	public static DJQrCodeFragment newInstance() {

		Bundle args = new Bundle();

		DJQrCodeFragment fragment = new DJQrCodeFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_qr_dj_code, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAuth = FirebaseAuth.getInstance();

		if (getView() != null && mAuth.getUid() != null) {
			final ImageView imageView_DJCode = getView().findViewById(R.id.imageView_DJCode);

			QRCodeWriter writer = new QRCodeWriter();
			String content = "realdj+" + mAuth.getUid();

			try {
				BitMatrix bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 512, 512);
				int width = bitMatrix.getWidth();
				int height = bitMatrix.getHeight();
				Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
				for (int x = 0; x < width; x++) {
					for (int y = 0; y < height; y++) {
						bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
					}
				}

				imageView_DJCode.setImageBitmap(bmp);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
