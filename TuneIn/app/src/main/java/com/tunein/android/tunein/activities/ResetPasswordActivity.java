package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.ResetPasswordFragment;
import com.tunein.android.tunein.interfaces.DismissActivity;

public class ResetPasswordActivity extends AppCompatActivity implements DismissActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reset_password);

		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle("Reset Password");
		}

		Intent startIntent = getIntent();

		if (startIntent != null && startIntent.getStringExtra("email") != null) {

			getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_10,
					ResetPasswordFragment.newInstance(startIntent.getStringExtra("email"))).commit();
		}
	}

	@Override
	public void dismissActivity() {
		finish();
	}
}
