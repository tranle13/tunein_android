package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.DJViewGenresFragment;
import com.tunein.android.tunein.interfaces.DismissActivity;

public class ViewGenreActivity extends AppCompatActivity implements DismissActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_genre);

		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle("Genres");
		}

		getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_14,
				DJViewGenresFragment.newInstance()).commit();
	}

	@Override
	public void dismissActivity() {
		Intent newIntent = new Intent(this, EditGenreActivity.class);
		startActivity(newIntent);
	}
}
