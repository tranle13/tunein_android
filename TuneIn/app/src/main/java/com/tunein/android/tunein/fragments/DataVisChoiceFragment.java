package com.tunein.android.tunein.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.interfaces.CallRespectiveActivity;

public class DataVisChoiceFragment extends Fragment implements View.OnClickListener {

	private CallRespectiveActivity mListener;

	public static DataVisChoiceFragment newInstance() {
		return new DataVisChoiceFragment();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if (context instanceof CallRespectiveActivity) {
			mListener = (CallRespectiveActivity)context;
		}
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_data_vis_choice, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null) {
			final Button weekly = getView().findViewById(R.id.button_Weekly);
			final Button monthly = getView().findViewById(R.id.button_Monthly);
			final Button yearly = getView().findViewById(R.id.button_Yearly);

			weekly.setOnClickListener(this);
			monthly.setOnClickListener(this);
			yearly.setOnClickListener(this);
		}
	}

	@Override
	public void onClick(View v) {
		if (mListener != null) {
			switch (v.getId()) {
				case R.id.button_Weekly:
					mListener.callRespectiveActivity(0, null);
					break;
				case R.id.button_Monthly:
					mListener.callRespectiveActivity(1, null);
					break;
				default:
					mListener.callRespectiveActivity(2, null);
					break;
			}
		}
	}
}
