package com.tunein.android.tunein.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.adapters.GenreListAdapter;
import com.tunein.android.tunein.interfaces.GetCustomGenres;

import java.util.ArrayList;
import java.util.Arrays;

public class GenreFragment extends Fragment {

	// Variables
	private final ArrayList<String> genres = new ArrayList<>(Arrays.asList("Ambient", "Bass", "Breakbeat", "Disco", "Drum 'n Bass", "Dubstep",
									"Electro", "Funk", "Heavy metal", "Hip hop & rap", "House", "Pop",
									"Punk rock", "Reggae", "Rock", "Techno", "Trance", "Trap"));
	private GetCustomGenres mListenerGenre;
	private EditText editText_CustomGenre;

	public static GenreFragment newInstance() {
		return new GenreFragment();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if (context instanceof GetCustomGenres) {
			mListenerGenre = (GetCustomGenres)context;
		}
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_genre, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getContext() != null) {
			editText_CustomGenre = getView().findViewById(R.id.editText_CustomGenre);
			final ListView listview_GenreList = getView().findViewById(R.id.listview_GenreList);
			GenreListAdapter adapter = new GenreListAdapter(genres, getContext());
			listview_GenreList.setAdapter(adapter);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_add, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_btn_add) {
			if (mListenerGenre != null) {
				mListenerGenre.getGenres(editText_CustomGenre.getText().toString());
			}
		}
		return super.onOptionsItemSelected(item);
	}
}
