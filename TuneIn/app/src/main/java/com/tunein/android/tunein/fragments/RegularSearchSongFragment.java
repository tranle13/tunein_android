package com.tunein.android.tunein.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.activities.HomeActivity;
import com.tunein.android.tunein.adapters.SongSearchAdapter;
import com.tunein.android.tunein.custom_objects.Track;
import com.tunein.android.tunein.helpers.MethodHelpers;
import com.tunein.android.tunein.interfaces.ViewLyrics;
import com.tunein.android.tunein.services.TrackIntentService;

import java.util.ArrayList;
import java.util.Objects;

public class RegularSearchSongFragment extends Fragment {

	// Member variables
	private ListView listView_SearchResult;
	private ProgressBar progress;
	private ConstraintLayout emptyState;
	private ViewLyrics mListenerActivity;
	private ArrayList<Track> results = new ArrayList<>();

	// Function to create new instance for fragment
	public static RegularSearchSongFragment newInstance() {
		return new RegularSearchSongFragment();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof ViewLyrics) {
			mListenerActivity = (ViewLyrics)context;
		}
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_regular_search_song, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getContext() != null && getActivity() != null) {
			listView_SearchResult = getView().findViewById(R.id.listView_SearchResult);
			final SearchView searchView_Search = getView().findViewById(R.id.searchView_Search);
			progress = getView().findViewById(R.id.progressBar_Search);
			emptyState = getView().findViewById(R.id.empty_state);

			searchView_Search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
				@Override
				public boolean onQueryTextSubmit(String s) {
					if (MethodHelpers.isConnected(getContext())) {
						Intent startIntent = new Intent(getContext(), TrackIntentService.class);
						startIntent.putExtra(HomeActivity.EXTRA_RESULT_RECEIVER, new TrackResultReceiver());
						startIntent.putExtra(HomeActivity.EXTRA_SEARCH_TERM, s);
						Objects.requireNonNull(getActivity()).startService(startIntent);
						progress.setVisibility(View.VISIBLE);
					} else {
						Toast.makeText(getContext(), "Please connect to the internet and try again", Toast.LENGTH_SHORT).show();
					}
					return false;
				}

				@Override
				public boolean onQueryTextChange(String s) {
					return false;
				}
			});

			searchView_Search.setOnCloseListener(new SearchView.OnCloseListener() {
				@Override
				public boolean onClose() {
					if (progress.getVisibility() == View.VISIBLE) {
						progress.setVisibility(View.GONE);
					}
					return false;
				}
			});

			listView_SearchResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					if (getActivity() != null && results != null && getContext() != null) {
						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

						LayoutInflater inflater = getActivity().getLayoutInflater();
						View dialogView = inflater.inflate(R.layout.custom_imageview_alert, null);
						ImageView imageView_Cover = dialogView.findViewById(R.id.imageView_Cover);
						Glide.with(getContext()).load(results.get(position).getCover()).into(imageView_Cover);
						builder.setView(dialogView);
						builder.setTitle(results.get(position).getName() + " - " + results.get(position).getArtist());
						final int chosenPosition = position;
						builder.setPositiveButton("View Lyrics", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								if (mListenerActivity != null) {
									mListenerActivity.viewLyrics(results.get(chosenPosition));
								}
							}
						});

						builder.setNegativeButton("Cancel", null);
						builder.create().show();
					}
				}
			});
		}
	}

	// Create handler and result receiver to communicate with main thread
	private final Handler mHandler = new Handler();

	public class TrackResultReceiver extends ResultReceiver {

		// Constructor
		public TrackResultReceiver() {
			super(mHandler);
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			super.onReceiveResult(resultCode, resultData);
			if (resultData != null) {
				progress.setVisibility(View.GONE);
				results = (ArrayList<Track>) resultData.getSerializable("RESULT");

				if (results != null) {
					if (results.size() > 0) {
						SongSearchAdapter adapter = new SongSearchAdapter(getContext(), results);
						listView_SearchResult.setAdapter(adapter);
						emptyState.setVisibility(View.GONE);
					} else {
						emptyState.setVisibility(View.VISIBLE);
					}
				} else {
					emptyState.setVisibility(View.VISIBLE);
				}
			}
		}
	}
}
