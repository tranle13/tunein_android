package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.DataVisChoiceFragment;
import com.tunein.android.tunein.interfaces.CallRespectiveActivity;

public class DataVisChoiceActivity extends AppCompatActivity implements CallRespectiveActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_data_vis_choice);

		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle("Choose Chart");
		}

		getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_12,
				DataVisChoiceFragment.newInstance()).commit();
	}

	@Override
	public void callRespectiveActivity(int which, String uid) {
		Intent startIntent = new Intent(this, DataVisualizationActivity.class);
		startIntent.putExtra("chart", which);
		startActivity(startIntent);
	}
}
