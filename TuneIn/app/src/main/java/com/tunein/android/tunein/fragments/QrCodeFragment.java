package com.tunein.android.tunein.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.adapters.QrCodePageAdapter;

public class QrCodeFragment extends Fragment {

	// Variables
	private static Boolean isDJ;

	// Code to create a new instance of the fragment
	public static QrCodeFragment newInstance(Boolean isDJOrNot) {

		Bundle args = new Bundle();
		isDJ = isDJOrNot;

		QrCodeFragment fragment = new QrCodeFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_qr, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getActivity() != null) {
			TabLayout tabLayout = getView().findViewById(R.id.tabLayout_Qr);

			tabLayout.addTab(tabLayout.newTab().setText("App QR Code"));

			if (isDJ) {
				tabLayout.addTab(tabLayout.newTab().setText("My QR Code"));
			} else {
				tabLayout.addTab(tabLayout.newTab().setText("QR Scanner"));
			}

			tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

			final ViewPager viewPager = getView().findViewById(R.id.viewPager);
			final QrCodePageAdapter adapter = new QrCodePageAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount(), isDJ);
			viewPager.setAdapter(adapter);
			viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
		}
	}
}
