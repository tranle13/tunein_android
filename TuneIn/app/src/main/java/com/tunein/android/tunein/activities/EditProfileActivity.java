package com.tunein.android.tunein.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.DJRegisterFragment;
import com.tunein.android.tunein.fragments.EditProfileFragment;
import com.tunein.android.tunein.interfaces.CallCamera;
import com.tunein.android.tunein.interfaces.DismissActivity;

import java.io.File;
import java.io.IOException;

public class EditProfileActivity extends AppCompatActivity implements CallCamera, DismissActivity {

	// Member variables
	private File imageFile;
	private static final int REQUEST_CAMERA = 0x00010;
	private static final String AUTHORITY = "com.tunein.android.imageprovider";
	private static final String IMAGE_FOLDER = "capture_images";
	private int genreRequestCode = 0x1101;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_profile);

		Intent startIntent = getIntent();

		if (startIntent != null) {
			boolean isDJ = startIntent.getBooleanExtra("isDJ", false);
			getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_5,
					EditProfileFragment.newInstance(isDJ)).commit();
		}
	}

	// Function to get Uri for file
	private Uri getOutputUri() {
		imageFile = getImageFile();

		if (imageFile == null) {
			return null;
		}

		return FileProvider.getUriForFile(this, AUTHORITY, imageFile.getAbsoluteFile());
	}

	// Function to create new file to store images
	private File getImageFile() {
		File protectedStorage = getExternalFilesDir(IMAGE_FOLDER);
		File imageFile = new File(protectedStorage, "profile_image");

		try {
			boolean canCreate = imageFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return imageFile;
	}

	@Override
	public void callCamera() {
		if (ContextCompat.checkSelfPermission(this,
				Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
			Intent photoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			photoIntent.putExtra(MediaStore.EXTRA_OUTPUT, getOutputUri());
			photoIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
			startActivityForResult(photoIntent, REQUEST_CAMERA);
		} else {
			ActivityCompat.requestPermissions(this, new String[]
					{Manifest.permission.CAMERA}, REQUEST_CAMERA);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		if (requestCode == REQUEST_CAMERA && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			Intent photoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			photoIntent.putExtra(MediaStore.EXTRA_OUTPUT, getOutputUri());
			photoIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
			startActivityForResult(photoIntent, REQUEST_CAMERA);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && data != null) {
			if (requestCode == REQUEST_CAMERA) {
				EditProfileFragment.chosenImage = imageFile;
			}
		}
	}

	@Override
	public void dismissActivity() {
		finish();
	}
}
