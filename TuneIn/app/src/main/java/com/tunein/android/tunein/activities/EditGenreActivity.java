package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.DJEditGenresFragment;
import com.tunein.android.tunein.interfaces.ChangeActivity;
import com.tunein.android.tunein.interfaces.DismissActivity;
import com.tunein.android.tunein.interfaces.GetChosenItems;

import java.util.ArrayList;

public class EditGenreActivity extends AppCompatActivity implements GetChosenItems, DismissActivity, ChangeActivity {

	private int genreRequestCode = 0x1101;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_genre);

		getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_15,
				DJEditGenresFragment.newInstance()).commit();

		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle("Edit Genres");
		}
	}

	@Override
	public void getChosenItems(ArrayList<String> items) {
		DJEditGenresFragment.chosenGenres = items;
	}

	@Override
	public void dismissActivity() {
		finish();
	}

	@Override
	public void changeActivity(Boolean isHome) {
		Intent toGenre = new Intent(this, GenreActivity.class);
		startActivityForResult(toGenre, genreRequestCode);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK && requestCode == genreRequestCode && data != null) {
			String[] newGenre = data.getStringExtra("genre").split(",");
			ArrayList<String> newGenres = new ArrayList<>();

			for (String music: newGenre) {
				newGenres.add(music.trim());
			}

			DJEditGenresFragment.newlyAddedGenres = newGenres;

		}
	}
}
