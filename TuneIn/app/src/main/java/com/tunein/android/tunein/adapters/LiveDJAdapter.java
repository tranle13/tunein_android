package com.tunein.android.tunein.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.custom_objects.Person;

import java.util.ArrayList;
import java.util.Objects;

public class LiveDJAdapter extends BaseAdapter {

	private final ArrayList<Person> djs;
	private final Context mContext;
	private static final int BASE_ID = 0x0011;
	private final String currentLiveDjUid;

	// Constructor
	public LiveDJAdapter(ArrayList<Person> _djs, Context _context, String _liveDjUid) {
		djs = _djs;
		mContext = _context;
		currentLiveDjUid = _liveDjUid;
	}

	@Override
	public int getCount() {
		if (djs != null) {
			return djs.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if (djs != null && 0 <= position || position < Objects.requireNonNull(djs).size()) {
			return djs.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position + BASE_ID;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LiveDJAdapter.ViewHolder vh;

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_livedj_listrow, parent,
					false);
			vh = new LiveDJAdapter.ViewHolder(convertView);
			convertView.setTag(vh);
		} else {
			vh = (LiveDJAdapter.ViewHolder)convertView.getTag();
		}

		if (djs.get(position) != null) {
			if (vh.imageView_IsCurrentLiveDj != null) {
				if (djs.get(position).getUid().equals(currentLiveDjUid)) {
					vh.imageView_IsCurrentLiveDj.setVisibility(View.VISIBLE);
				} else {
					vh.imageView_IsCurrentLiveDj.setVisibility(View.GONE);
				}
			}
			vh.textView_DJName.setText(djs.get(position).getFullName());
			Glide.with(mContext).load(djs.get(position).getImage()).into(vh.imageview_DJImage);
		}

		return convertView;
	}

	// Recycle view pattern
	static class ViewHolder {
		final ImageView imageview_DJImage;
		final TextView textView_DJName;
		final ImageView imageView_IsCurrentLiveDj;

		ViewHolder(View layout) {
			imageview_DJImage = layout.findViewById(R.id.imageView_RegularDJImage);
			textView_DJName = layout.findViewById(R.id.textView_DJName);
			imageView_IsCurrentLiveDj = layout.findViewById(R.id.imageView_CurrentLiveDj);
		}
	}
}
