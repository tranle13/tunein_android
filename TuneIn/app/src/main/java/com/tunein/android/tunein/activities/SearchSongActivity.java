package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.custom_objects.Track;
import com.tunein.android.tunein.fragments.RegularSearchSongFragment;
import com.tunein.android.tunein.helpers.MethodHelpers;
import com.tunein.android.tunein.interfaces.RequestSong;
import com.tunein.android.tunein.interfaces.ViewLyrics;

public class SearchSongActivity extends AppCompatActivity implements RequestSong, ViewLyrics {

	// Member variable
	private FirebaseAuth mAuth;
	private DatabaseReference mRef;
	public static final String TAG = "SearchSongActivity";
	public static final String EXTRA_CHOSEN_TRACK = "EXTRA_CHOSEN_TRACK";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_song);

		getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_7,
				RegularSearchSongFragment.newInstance()).commit();

		mAuth = FirebaseAuth.getInstance();
		mRef = FirebaseDatabase.getInstance().getReference();
	}

	@Override
	public void requestSong(final Track chosenTrack) {
		if (mAuth.getUid() != null && MethodHelpers.isConnected(this)) {
			mRef.child("regular_users").child(mAuth.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

					String liveDJUid = (String)dataSnapshot.child("currentLiveDJ").getValue();
					if (liveDJUid != null) {
						mRef.child("regular_users").child(mAuth.getUid()).child("requested_song").child("artist").setValue(chosenTrack.getArtist());
						mRef.child("regular_users").child(mAuth.getUid()).child("requested_song").child("title").setValue(chosenTrack.getName());
						mRef.child("regular_users").child(mAuth.getUid()).child("requested_song").child("url").setValue(chosenTrack.getCover());
						mRef.child("dj_users").child(liveDJUid).child("requested_songs").child(mAuth.getUid())
								.setValue(chosenTrack.getName());
						finish();
					} else {
						Toast.makeText(SearchSongActivity.this, "Please select a live DJ first", Toast.LENGTH_SHORT).show();
					}
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.w(TAG, "onCancelled: ", databaseError.toException());
				}
			});
		} else {
			Toast.makeText(this, "Please check your internet connection and try again", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void viewLyrics(Track track) {
		Intent viewLyricsIntent = new Intent(this, SongLyricsActivity.class);
		viewLyricsIntent.putExtra(EXTRA_CHOSEN_TRACK, track);
		startActivity(viewLyricsIntent);
	}
}
