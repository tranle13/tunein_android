package com.tunein.android.tunein.interfaces;

public interface GetCustomGenres {
	void getGenres(String customGenre);
}
