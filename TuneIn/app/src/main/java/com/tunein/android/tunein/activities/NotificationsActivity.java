package com.tunein.android.tunein.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.NotificationFragment;

public class NotificationsActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notifications);

		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle("Notifications");
		}

		if (getIntent() != null) {
			getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_13,
					NotificationFragment.newInstance(getIntent().getBooleanExtra("isDJ", false))).commit();
		}
	}
}
