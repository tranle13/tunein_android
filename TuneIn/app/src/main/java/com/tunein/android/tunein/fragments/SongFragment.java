package com.tunein.android.tunein.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.adapters.RequestedSongAdapter;
import com.tunein.android.tunein.custom_objects.Track;
import com.tunein.android.tunein.interfaces.CallRespectiveActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class SongFragment extends Fragment {

	// Member variables
	private static Boolean isDJ;
	private DatabaseReference mRef;
	private FirebaseAuth mAuth;
	private CallRespectiveActivity mListenerSearchActivity;
	private static final String TAG = "SongFragment";

	// Initializer
	public static SongFragment newInstance(Boolean isDJOrNot) {

		Bundle args = new Bundle();

		isDJ = isDJOrNot;

		SongFragment fragment = new SongFragment();
		fragment.setArguments(args);
		return fragment;
	}

	// Check if host context implements the right interface
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if (context instanceof CallRespectiveActivity) {
			mListenerSearchActivity = (CallRespectiveActivity)context;
		}
	}

	// Set up options menu
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (!isDJ) {
			setHasOptionsMenu(true);
		}
	}

	// Use the right layout if it's DJ and if it's not DJ
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if (isDJ) {
			return inflater.inflate(R.layout.fragment_song_dj, container, false);
		} else {
			return inflater.inflate(R.layout.fragment_song_regular, container, false);
		}
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mRef = FirebaseDatabase.getInstance().getReference();
		mAuth = FirebaseAuth.getInstance();

		if (getView() != null && mAuth.getUid() != null && getContext() != null) {
			if (!isDJ) {
				final ImageView imageView_TrackCover = getView().findViewById(R.id.imageView_SongCover);
				final TextView textView_SongName = getView().findViewById(R.id.textView_SongName);
				final TextView textView_SongArtist = getView().findViewById(R.id.textView_SongArtist);
				final ConstraintLayout notEmptyState = getView().findViewById(R.id.none_empty_state);
				final ConstraintLayout emptyState = getView().findViewById(R.id.empty_state);

				mRef.child("regular_users").child(mAuth.getUid()).addValueEventListener(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						if (dataSnapshot.child("currentLiveDJ").getValue() != null && dataSnapshot.child("requested_song").getValue() != null) {
							String title = (String)dataSnapshot.child("requested_song").child("title").getValue();
							String artist = (String)dataSnapshot.child("requested_song").child("artist").getValue();
							String url = (String)dataSnapshot.child("requested_song").child("url").getValue();

							if (title != null && artist != null && url != null) {
								textView_SongName.setText(title);
								textView_SongArtist.setText(artist);
								if (getContext() != null) {
									Glide.with(getContext()).load(url).into(imageView_TrackCover);
								}
								emptyState.setVisibility(View.GONE);
								notEmptyState.setVisibility(View.VISIBLE);
							}
						} else {
							emptyState.setVisibility(View.VISIBLE);
							notEmptyState.setVisibility(View.GONE);
						}
					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
						Log.w(TAG, "onCancelled: ", databaseError.toException());
					}
				});
			} else {
				final ListView listView_SongRequests = getView().findViewById(R.id.listview_SongRequest);
				final ConstraintLayout emptyState = getView().findViewById(R.id.empty_state);

				mRef.child("dj_users").child(mAuth.getUid()).child("requested_songs").addValueEventListener(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						ArrayMap<String, String> requestedSongs = new ArrayMap<>();
						for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
							if (snapshot.getValue() != null) {
								requestedSongs.put(snapshot.getKey(), snapshot.getValue().toString());
							}
						}

						if (requestedSongs.size() > 0) {
							RequestedSongAdapter adapter = new RequestedSongAdapter(getContext(), requestedSongs);
							listView_SongRequests.setAdapter(adapter);
							emptyState.setVisibility(View.GONE);
						} else {
							emptyState.setVisibility(View.VISIBLE);
						}
					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
						Log.w(TAG, "onCancelled: ", databaseError.toException());
					}
				});
			}
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_search, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_btn_search) {
			if (mListenerSearchActivity != null) {
				mListenerSearchActivity.callRespectiveActivity(item.getItemId(), null);
			}
		}
		return super.onOptionsItemSelected(item);
	}
}
