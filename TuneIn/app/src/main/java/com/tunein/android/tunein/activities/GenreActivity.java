package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.GenreFragment;
import com.tunein.android.tunein.interfaces.GetCustomGenres;
import com.tunein.android.tunein.interfaces.GetChosenItems;

import java.util.ArrayList;

public class GenreActivity extends AppCompatActivity implements GetCustomGenres, GetChosenItems {

	// Variable
	private ArrayList<String> itemList = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_genre);

		getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_4,
				GenreFragment.newInstance()).commit();
	}

	@Override
	public void getGenres(String customGenre) {
		StringBuilder genreBuilder = new StringBuilder();

		if (customGenre.length() > 0) {
			String[] customGenres = customGenre.split(",");

			for (String genre: customGenres) {
				String trimmedGenre;
				String checkGenre = genre.replace(" ", "");

				if (!checkGenre.isEmpty()) {
					if (itemList.size() > 0) {
						trimmedGenre = genre.trim() + ", ";
					} else {
						trimmedGenre = genre.trim();
					}

					genreBuilder.append(trimmedGenre);
				}
			}
		}

		for (int i = 0; i < itemList.size(); i++) {
			String allGenres;
			if (i < itemList.size() - 1) {
				allGenres = itemList.get(i) + ", ";
			} else {
				allGenres = itemList.get(i);
			}

			genreBuilder.append(allGenres);
		}

		Intent backToRegister = new Intent();
		backToRegister.putExtra("genre", genreBuilder.toString());
		setResult(RESULT_OK, backToRegister);
		finish();
	}

	@Override
	public void getChosenItems(ArrayList<String> items) {
		itemList = items;
	}
}
