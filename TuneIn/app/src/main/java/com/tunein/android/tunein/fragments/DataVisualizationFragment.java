package com.tunein.android.tunein.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Column;
import com.anychart.enums.Anchor;
import com.anychart.enums.HoverMode;
import com.anychart.enums.Position;
import com.anychart.enums.TooltipPositionMode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tunein.android.tunein.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class DataVisualizationFragment extends Fragment {

	// Variables
	private static int weekMonthOrYear;
	private DatabaseReference mRef;
	private FirebaseAuth mAuth;
	private static final String TAG = "DataVisualizationFrag";
	private ArrayMap<Integer, ArrayList<Integer>> weekDays = new ArrayMap<>();
	private ArrayMap<Integer, Integer> weekly = new ArrayMap<>();
	private ArrayMap<Integer, Integer> monthly = new ArrayMap<>();
	private ArrayMap<Integer, Integer> yearly = new ArrayMap<>();

	// Initializer
	public static DataVisualizationFragment newInstance(int _weekMonthOrYear) {
		weekMonthOrYear = _weekMonthOrYear;
		return new DataVisualizationFragment();
	}

	// Return the layout for the fragment
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_data_visualization, container, false);
	}

	// Deal with data to produce the chart
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAuth = FirebaseAuth.getInstance();

		if (getView() != null && mAuth.getUid() != null) {
			mRef = FirebaseDatabase.getInstance().getReference().child("dj_users").child(mAuth.getUid()).child("tips");
			final AnyChartView chartView = getView().findViewById(R.id.chartView);
			final ProgressBar progressBar = getView().findViewById(R.id.progressBar);

			progressBar.setVisibility(View.VISIBLE);

			Date today = Calendar.getInstance().getTime();
			SimpleDateFormat format = new SimpleDateFormat("dd_MM_yyyy");
			String strDate = format.format(today);
			final String[] dateTime = strDate.split("_");
			final Integer currentMonth = Integer.valueOf(dateTime[1]);
			final Integer currentYear = Integer.valueOf(dateTime[2]);
			int numOfWeeks = 4;

			if (weekMonthOrYear == 0) {
				try {
					Date date = format.parse(strDate);
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(date);

					numOfWeeks = calendar.getActualMaximum(Calendar.WEEK_OF_MONTH);
					int lastDate = calendar.getActualMaximum(Calendar.DATE);

					for (int i = 0; i < numOfWeeks; i++) {
						int chosenDay = 1 + (7 * i);

						if (chosenDay > lastDate) {
							chosenDay = lastDate;
						}

						String stringDate = chosenDay + "_" + currentMonth + "_" + currentYear;
						Date theDate = format.parse(stringDate);
						Calendar newCalendar = Calendar.getInstance();
						newCalendar.setTime(theDate);
						int dayInWeek = newCalendar.get(Calendar.DAY_OF_WEEK);
						int startDay;
						int endDay;
						int distanceToStart;
						int distanceToEnd = 0;

						if (dayInWeek == 1) {
							distanceToStart = 6;
						} else {
							distanceToStart = dayInWeek - 2;
							distanceToEnd = 8 - dayInWeek;
						}

						startDay = chosenDay - distanceToStart;
						endDay = chosenDay + distanceToEnd;

						if (startDay < 0) {
							startDay = 1;
						}

						if (endDay > lastDate) {
							endDay = lastDate;
						}

						Integer[] days = {startDay, endDay};
						ArrayList<Integer> _days = new ArrayList<Integer>();
						_days.add(startDay);
						_days.add(endDay);
						weekDays.put(i+1, _days);

					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			String time = "0";
			if (weekMonthOrYear == 0) {
				for (int i = 0; i < numOfWeeks; i++) {
					Integer num = Integer.valueOf(time + (i + 1));
					weekly.put(num, 0);
				}
			} else {
				for (int i = 0; i < 12; i++) {
					Integer num = Integer.valueOf(time + (i + 1));
					monthly.put(num, 0);
				}
			}

			mRef.addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
						for (DataSnapshot subSnapshot: snapshot.getChildren()) {
							if (subSnapshot.getKey() != null && subSnapshot.getValue() != null) {
								int amount = ((Long) subSnapshot.getValue()).intValue();
								String[] savedDateTime = subSnapshot.getKey().split("_");
								Integer day = Integer.valueOf(savedDateTime[0]);
								Integer month = Integer.valueOf(savedDateTime[1]);
								Integer year = Integer.valueOf(savedDateTime[2]);
								int total = 0;
								switch (weekMonthOrYear) {
									case 0:
										if (month.equals(currentMonth) && year.equals(currentYear)) {
											int week = checkWhichWeek(day);

											if (weekly.containsKey(week) && weekly.get(week) != null) {
												total = weekly.get(week);
											}

											weekly.setValueAt(week - 1, total + amount);
										}
										break;
									case 1:
										if (year.equals(currentYear)) {
											if (monthly.containsKey(month) && monthly.get(month) != null) {
												total = monthly.get(month);
											}

											monthly.put(month, total + amount);
										}
										break;
									default:
										if (yearly.containsKey(year) && yearly.get(year) != null) {
											total = yearly.get(year);
										}

										yearly.put(year, total + amount);
										break;
								}
							}
						}
					}

					List<DataEntry> data = new ArrayList<>();
					Cartesian cartesian = AnyChart.column();
					switch (weekMonthOrYear) {
						case 0:
							for (int i = 0; i < weekly.size(); i++) {
								data.add(new ValueDataEntry(weekly.keyAt(i), weekly.get(i + 1)));
							}
							cartesian.xAxis(0).title("Week");
							cartesian.title("Tip Revenue Distribution on Weekly Basics");
							break;
						case 1:
							for (int i = 0; i < monthly.size(); i++) {
								data.add(new ValueDataEntry(monthly.keyAt(i), monthly.get(i + 1)));
							}
							cartesian.xAxis(0).title("Month");
							cartesian.title("Tip Revenue Distribution on Monthly Basics");
							break;
						default:
							for (int i = 0; i < yearly.size(); i++) {
								data.add(new ValueDataEntry(yearly.keyAt(i), yearly.get(yearly.keyAt(i))));
							}
							cartesian.xAxis(0).title("Year");
							cartesian.title("Tip Revenue Distribution on Yearly Basics");
							break;
					}


					progressBar.setVisibility(View.GONE);
					Column column = cartesian.column(data);

					column.tooltip()
							.titleFormat("{%X}")
							.position(Position.CENTER_BOTTOM)
							.anchor(Anchor.CENTER_BOTTOM)
							.offsetX(0d)
							.offsetY(5d)
							.format("${%Value}{groupsSeparator: }");

					cartesian.animation(true);

					cartesian.yScale().minimum(0d);

					cartesian.yAxis(0).labels().format("${%Value}{groupsSeparator: }");

					cartesian.tooltip().positionMode(TooltipPositionMode.POINT);
					cartesian.interactivity().hoverMode(HoverMode.BY_X);

					cartesian.yAxis(0).title("Tip Amount");

					chartView.setChart(cartesian);
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.w(TAG, "onCancelled: ", databaseError.toException());
				}
			});
		}
	}

	private int checkWhichWeek(Integer date) {
		int week = 1;

		for (int i = 0; i < weekDays.size(); i++) {
			if (Objects.requireNonNull(weekDays.get(i + 1)).contains(date)) {
				week = i + 1;
			}
		}

		return week;
	}
}
