package com.tunein.android.tunein.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.adapters.ApprovedSongAdapter;
import com.tunein.android.tunein.adapters.LiveDJAdapter;
import com.tunein.android.tunein.custom_objects.Person;
import com.tunein.android.tunein.custom_objects.Track;
import com.tunein.android.tunein.helpers.MethodHelpers;
import com.tunein.android.tunein.interfaces.CallRespectiveActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class HomeFragment extends Fragment implements View.OnClickListener, LocationListener {

	// Variables
	private ArrayList<Track> acceptedRequests = new ArrayList<>();
	private static Boolean isDJ = false;
	private FirebaseAuth mAuth;
	private DatabaseReference mRef;
	private TextView textView_DJLocation;
	private ArrayList<Track> approvedSongs;
	private Switch switch_GoingLive;

	private static final int REQUEST_LOCATION_PERMISSIONS = 0x01101;
	private LocationManager mLocationManager;
	private boolean mRequestingUpdates = false;
	private static final String TAG = "HomeFragment";
	private ArrayList<Person> nearbyLiveDjs = new ArrayList<>();
	private Location regularCurrentLocation;
	private ProgressBar progress;
	private ListView listView_LiveDJs;
	private String currentLiveDJ;
	private ConstraintLayout regularEmptyState;
	private CallRespectiveActivity mListenerActivity;
	private ConstraintLayout emptyState;

	// Create new instance of fragment
	public static HomeFragment newInstance(Boolean isDJOrNot) {
		Bundle args = new Bundle();

		isDJ = isDJOrNot;
		HomeFragment fragment = new HomeFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof CallRespectiveActivity) {
			mListenerActivity = (CallRespectiveActivity)context;
		}
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isDJ) {
			setHasOptionsMenu(true);
		}
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if (isDJ) {
			return inflater.inflate(R.layout.fragment_home_dj, container, false);
		} else {
			return inflater.inflate(R.layout.fragment_home_regular, container, false);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mAuth.getUid() != null) {
			getCurrentLocation(null, mAuth.getUid());
			mRef.child("dj_users").child(mAuth.getUid()).addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

					Boolean isLive = (Boolean) dataSnapshot.child("isLive").getValue();
					if (isLive != null) {
						switch_GoingLive.setChecked(isLive);
						if (isLive) {
							getCurrentLocation(textView_DJLocation, mAuth.getUid());
						} else {
							textView_DJLocation.setVisibility(View.INVISIBLE);
							emptyState.setVisibility(View.VISIBLE);
						}
					}
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.i(TAG, "onCancelled: Process cancelled");
				}
			});
		}
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAuth = FirebaseAuth.getInstance();
		mRef = FirebaseDatabase.getInstance().getReference();

		if (getView() != null && getActivity() != null && mAuth.getUid() != null && getContext() != null) {
			if (isDJ) {

				final ListView listview_AcceptedRequest = getView().findViewById(R.id.listview_AcceptedRequest);
				final TextView textView_DJName = getView().findViewById(R.id.textView_DJName);
				switch_GoingLive = getView().findViewById(R.id.switch_GoLive);
				textView_DJLocation = getView().findViewById(R.id.textView_DJLocation);
				final ImageView imageView_DJImage = getView().findViewById(R.id.imageview_DJImage);
				emptyState = getView().findViewById(R.id.empty_state);

				// Code to see if DJ has any requests
				if (acceptedRequests.size() > 0) {
					emptyState.setVisibility(View.GONE);
				} else {
					emptyState.setVisibility(View.VISIBLE);
				}

				// Code to get all the approved songs
				final ValueEventListener approvedSongListener = new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						approvedSongs = new ArrayList<>();
						for (DataSnapshot snapshot: dataSnapshot.child("approved_songs").getChildren()) {
							for (DataSnapshot subSnapshot: snapshot.getChildren()) {
								approvedSongs.add(new Track(subSnapshot.getKey(), (String)subSnapshot.getValue(), null));
							}
						}

						if (approvedSongs.size() > 0) {
							ApprovedSongAdapter adapter = new ApprovedSongAdapter(getContext(), approvedSongs);
							listview_AcceptedRequest.setAdapter(adapter);
							emptyState.setVisibility(View.GONE);
						} else {
							emptyState.setVisibility(View.VISIBLE);
						}


					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) { }
				};

				// Code to get DJ name
				mRef.child("dj_users").child(mAuth.getUid()).addValueEventListener(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						if (dataSnapshot.child("fullname").getValue() != null) {
							textView_DJName.setText(Objects.requireNonNull(dataSnapshot.child("fullname").getValue()).toString());
						} else {
							textView_DJName.setText(R.string.no_name);
						}

						String image = (String)dataSnapshot.child("image").getValue();

						if (imageView_DJImage != null && getContext() != null) {
							Glide.with(getContext()).load(image).into(imageView_DJImage);
						}

						Boolean isLive = (Boolean)dataSnapshot.child("isLive").getValue();
						if (isLive != null) {
							switch_GoingLive.setChecked(isLive);
							if (isLive) {
								getCurrentLocation(textView_DJLocation, mAuth.getUid());
								mRef.child("dj_users").child(Objects.requireNonNull(mAuth.getUid())).addValueEventListener(approvedSongListener);
							} else {
								textView_DJLocation.setVisibility(View.INVISIBLE);
								emptyState.setVisibility(View.VISIBLE);
							}
						}
					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
						Log.i(TAG, "onCancelled: Process cancelled");
					}
				});

				// Code to get DJ current location when they go live
				switch_GoingLive.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (switch_GoingLive.isChecked()) {
							AlertDialog.Builder building = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
							building.setMessage("Are you sure you want to go live?");

							building.setPositiveButton("OK", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {

									getCurrentLocation(textView_DJLocation, mAuth.getUid());
									mRef.child("dj_users").child(Objects.requireNonNull(mAuth.getUid())).addValueEventListener(approvedSongListener);
								}
							});

							building.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									switch_GoingLive.setChecked(false);
								}
							});
							building.create().show();
						} else {
							textView_DJLocation.setVisibility(View.INVISIBLE);
							mLocationManager.removeUpdates(HomeFragment.this);

							mRef.child("dj_users").child(mAuth.getUid()).child("isLive").setValue(false);
							mRef.child("dj_users").child(mAuth.getUid()).child("latitude").removeValue();
							mRef.child("dj_users").child(mAuth.getUid()).child("longitude").removeValue();
							mRef.child("dj_users").child(mAuth.getUid()).child("city").removeValue();
							mRef.child("dj_users").child(mAuth.getUid()).child("state").removeValue();

							mRef.child("dj_users").child(Objects.requireNonNull(mAuth.getUid())).removeEventListener(approvedSongListener);
						}
					}
				});



			} else {

				listView_LiveDJs = getView().findViewById(R.id.listview_CurrentLiveDJs);
				progress = getView().findViewById(R.id.progress);
				regularEmptyState = getView().findViewById(R.id.empty_state);

				mRef.child("regular_users").child(mAuth.getUid()).child("currentLiveDJ").addValueEventListener(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						currentLiveDJ = (String)dataSnapshot.getValue();
					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
						Log.w(TAG, "onCancelled: ", databaseError.toException());
					}
				});

				getCurrentLocation(null, mAuth.getUid());

				listView_LiveDJs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						if (mListenerActivity != null) {
							mListenerActivity.callRespectiveActivity(R.id.listview_CurrentLiveDJs, nearbyLiveDjs.get(position).getUid());
						}
					}
				});
			}
		}
	}

	private ValueEventListener nearbyDJListener = new ValueEventListener() {
		@Override
		public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
			progress.setVisibility(View.VISIBLE);
			nearbyLiveDjs.clear();

			for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
				Boolean isLive = (Boolean)snapshot.child("isLive").getValue();
				Double latitude = (Double) snapshot.child("latitude").getValue();
				Double longitude = (Double)snapshot.child("longitude").getValue();

				if (isLive != null && isLive && latitude != null && longitude != null) {

					Location djLocation = new Location(LocationManager.GPS_PROVIDER);
					djLocation.setLatitude(latitude);
					djLocation.setLongitude(longitude);

					if (regularCurrentLocation != null) {
						float distance = regularCurrentLocation.distanceTo(djLocation);
						if (distance <= 3500d && snapshot.getKey() != null) {

							final String fullname = (String) snapshot.child("fullname").getValue();
							final String email = (String) snapshot.child("email").getValue();
							final String image = (String)snapshot.child("image").getValue();

							nearbyLiveDjs.add(new Person(fullname, snapshot.getKey(), email, isDJ, image));
						}
					}
				}
			}

			progress.setVisibility(View.GONE);
			if (nearbyLiveDjs.size() > 0) {
				LiveDJAdapter adapter = new LiveDJAdapter(nearbyLiveDjs, getContext(), currentLiveDJ);
				listView_LiveDJs.setAdapter(adapter);
				regularEmptyState.setVisibility(View.GONE);
			} else {
				regularEmptyState.setVisibility(View.VISIBLE);
			}
		}

		@Override
		public void onCancelled(@NonNull DatabaseError databaseError) {
			Log.w(TAG, "onCancelled: ", databaseError.toException());
		}
	};

	// Code to update location
	@Override
	public void onLocationChanged(Location location) {
		Double latitude = location.getLatitude();
		Double longitude = location.getLongitude();

		if (mRequestingUpdates) {
			mRequestingUpdates = false;
			mLocationManager.removeUpdates(this);
		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) { }
	@Override
	public void onProviderEnabled(String provider) { }
	@Override
	public void onProviderDisabled(String provider) { }

	@Override
	public void onClick(View v) {
		// Check to see if we have permission that we're not already requesting updates
		if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

			// Request location updates using 'this' as our locationListener
			if (!isDJ) {
				mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 15000, 3500.0f, this);
			} else {
				mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 3500.0f, this);
			}

			// Track that we're requesting updates so we don't request them twice
			mRequestingUpdates = true;
		}
	}

	private void getCurrentLocation(TextView djLocation, String uid) {
		if (getActivity() != null) {
			mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

			if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

				// GPS_PROVIDER for emulator and NETWORK_PROVIDER for actual devices
				Location lastKnown = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				if (lastKnown != null) {
					Double latitude = lastKnown.getLatitude();
					Double longitude = lastKnown.getLongitude();

					if (isDJ) {

						mRef.child("dj_users").child(uid).child("isLive").setValue(true);
						mRef.child("dj_users").child(uid).child("latitude").setValue(latitude);
						mRef.child("dj_users").child(uid).child("longitude").setValue(longitude);

						// Code to get the true address
						Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());

						try {
							List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

							if (addresses != null) {
								StringBuilder addressBuilder = new StringBuilder();

								for (int i = 0; i <= addresses.get(0).getMaxAddressLineIndex(); i++) {
									addressBuilder.append(addresses.get(0).getAddressLine(i)).append("\n");
								}

								mRef.child("dj_users").child(uid).child("city").setValue(addresses.get(0).getSubAdminArea());
								mRef.child("dj_users").child(uid).child("state").setValue(addresses.get(0).getAdminArea());
								djLocation.setText(addressBuilder.toString());
								djLocation.setVisibility(View.VISIBLE);

							} else {
								Log.w("My Current location", "No Address returned!");
							}
						} catch (Exception e) {
							e.printStackTrace();
							Log.w("My Current location", "Cannot get address!");
						}
					} else {
						regularCurrentLocation = new Location(LocationManager.GPS_PROVIDER);
						regularCurrentLocation.setLatitude(latitude);
						regularCurrentLocation.setLongitude(longitude);
						mRef.child("dj_users").addValueEventListener(nearbyDJListener);
					}
				} else {
					double latitude = 0d;
					double longitude = 0d;
					regularCurrentLocation = new Location(LocationManager.GPS_PROVIDER);
					regularCurrentLocation.setLatitude(latitude);
					regularCurrentLocation.setLongitude(longitude);
				}
			} else {
				requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSIONS);
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			if (isDJ) {
				getCurrentLocation(textView_DJLocation, mAuth.getUid());
			} else {
				getCurrentLocation(null, mAuth.getUid());
			}
		} else {
			if (isDJ) {
				switch_GoingLive.setChecked(false);
			} else {
				regularEmptyState.setVisibility(View.VISIBLE);
			}
		}

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_delete, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_btn_delete && mAuth.getUid() != null) {
			mRef.child("dj_users").child(mAuth.getUid()).child("approved_songs").removeValue();
			emptyState.setVisibility(View.VISIBLE);
		}
		return super.onOptionsItemSelected(item);
	}



}
