package com.tunein.android.tunein.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.interfaces.CallRespectiveActivity;

public class QrScannerFragment extends Fragment {

	private static final int REQUEST_CAMERA = 0x01011;

	private CameraSource cameraSource;
	private BarcodeDetector barcodeDetector;
	private SurfaceHolder selfHolder;
	private TextView textView_invalidCode;
	private CallRespectiveActivity mListenerActivity;
	private boolean didFindDJ = true;
	private String qrCode;

	public static QrScannerFragment newInstance() {

		Bundle args = new Bundle();

		QrScannerFragment fragment = new QrScannerFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getActivity() != null) {
			barcodeDetector = new BarcodeDetector.Builder(getActivity()).setBarcodeFormats(Barcode.QR_CODE).build();
			cameraSource = new CameraSource.Builder(getActivity(), barcodeDetector).setRequestedPreviewSize(640, 480).build();
		}
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if (context instanceof CallRespectiveActivity) {
			mListenerActivity = (CallRespectiveActivity)context;
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		if (!didFindDJ) {
			didFindDJ = true;
		}
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_qr_scanner, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getActivity() != null && getContext() != null) {
			textView_invalidCode = getView().findViewById(R.id.textView_InvalidCode);

			final SurfaceView scanner = getView().findViewById(R.id.scanner);
			scanner.getHolder().addCallback(new SurfaceHolder.Callback() {
				@Override
				public void surfaceCreated(SurfaceHolder holder) {
					selfHolder = holder;
					if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

						requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
						return;
					}

					try {
						cameraSource.start(holder);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				@Override
				public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) { }

				@Override
				public void surfaceDestroyed(SurfaceHolder holder) {
					cameraSource.stop();
				}
			});

			barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
				@Override
				public void release() { }

				@Override
				public void receiveDetections(Detector.Detections<Barcode> detections) {
					final SparseArray<Barcode> barcodeSparseArray = detections.getDetectedItems();

					if (barcodeSparseArray.size() > 0) {
						qrCode = barcodeSparseArray.valueAt(0).rawValue;
						String uid = qrCode.substring(7);
						if (qrCode.contains("realdj") && didFindDJ) {
							if (mListenerActivity != null) {
								mListenerActivity.callRespectiveActivity(R.id.scanner, uid);
								didFindDJ = false;
							}
							textView_invalidCode.setVisibility(View.GONE);
						} else {
							textView_invalidCode.setText(R.string.cannot_detect_qr);
							textView_invalidCode.setVisibility(View.VISIBLE);
						}
					}
				}
			});
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && getContext() != null) {
			try {
				if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
					return;
				}
				textView_invalidCode.setVisibility(View.GONE);
				cameraSource.start(selfHolder);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			textView_invalidCode.setText(R.string.enable_camera);
			textView_invalidCode.setVisibility(View.VISIBLE);
		}
	}
}
