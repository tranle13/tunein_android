package com.tunein.android.tunein.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.helpers.MethodHelpers;
import com.tunein.android.tunein.interfaces.CallCamera;
import com.tunein.android.tunein.interfaces.ChangeActivity;

import java.util.Objects;
import java.util.regex.Pattern;

public class DJRegisterFragment extends Fragment {

	// Member variables
	private FirebaseAuth mAuth;
	private ChangeActivity mListener;
	private EditText editText_Genre;
	private ImageView imageView_ProfilePicture;
	public static String genre = "";
	public static final int REQUEST_IMAGE = 0x1010;
	private CallCamera mListenerCam;
	public static Bitmap capturedImage;

	public static DJRegisterFragment newInstance() {
		return new DJRegisterFragment();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAuth = FirebaseAuth.getInstance();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if (context instanceof ChangeActivity && context instanceof CallCamera) {
			mListener = (ChangeActivity)context;
			mListenerCam = (CallCamera)context;
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		if (editText_Genre != null) {
			editText_Genre.setText(genre);
		}

		if (capturedImage != null) {
			imageView_ProfilePicture.setImageBitmap(capturedImage);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		genre = null;
		capturedImage = null;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_dj_registration, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getContext() != null && getActivity() != null) {

			// Code to get UI components, their values and carry out respective action
			final EditText editText_Email = getView().findViewById(R.id.editText_Email);
			final EditText editText_FullName = getView().findViewById(R.id.editText_FullName);
			final EditText editText_Password = getView().findViewById(R.id.editText_Password);
			editText_Genre = getView().findViewById(R.id.editText_Genre);
			final EditText editText_ConfirmPass = getView().findViewById(R.id.editText_ConfirmPass);
			final Button button_Register = getView().findViewById(R.id.button_Register);
			final Switch switch_AndroidPay = getView().findViewById(R.id.switch_AndroidPay);
			imageView_ProfilePicture = getView().findViewById(R.id.imageView_ProfileImage);

			imageView_ProfilePicture.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					MethodHelpers.createGalleryCameraAlert(getActivity(), mListenerCam, REQUEST_IMAGE);
				}
			});

			switch_AndroidPay.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					AlertDialog.Builder building = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
					building.setTitle("Tune-In Would Like To Access Your Android Pay");
					building.setMessage("This allows user to tip the DJ faster and more convenient");

					building.setPositiveButton("OK", null);

					building.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							switch_AndroidPay.setChecked(false);
						}
					});
					building.create().show();
				}
			});

			editText_Genre.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mListener != null) {
						mListener.changeActivity(false);
					}
				}
			});

			button_Register.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (MethodHelpers.isConnected(getContext())) {
						final String emailStr = editText_Email.getText().toString();
						final String genreStr = editText_Genre.getText().toString();
						final String passwordStr = editText_Password.getText().toString();
						final String fullNameStr = editText_FullName.getText().toString();
						final String confirmPassStr = editText_ConfirmPass.getText().toString();

						int clear = MethodHelpers.checkIfTextsEmptyOrSpaced(emailStr, passwordStr, fullNameStr, confirmPassStr, 1);
						if (clear == 0) {
							final boolean isAtLeast6 = passwordStr.length() >= 6;
							Pattern regex = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$");
							final boolean containRequiredCharacters = regex.matcher(passwordStr).matches();

							mAuth.fetchSignInMethodsForEmail(emailStr).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
								@Override
								public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
									if (task.isSuccessful() && task.getResult().getSignInMethods().isEmpty()) {
										if (isAtLeast6 && containRequiredCharacters) {

											AlertDialog.Builder building = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
											building.setMessage("Do you agree that we can take 20% off each tip transaction? Refusing " +
													"this means you cannot create a DJ account");

											building.setPositiveButton("OK", new DialogInterface.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {
													if (MethodHelpers.isConnected(getContext())) {
														Bitmap profilePicture = ((BitmapDrawable) imageView_ProfilePicture.getDrawable()).getBitmap();
														MethodHelpers.createNewAccount(mAuth, getActivity(), mListener, getContext(), true,
																emailStr, passwordStr, genreStr, fullNameStr, profilePicture, false, null);
													} else {
														Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
													}
												}
											});

											building.setNegativeButton("Cancel", null);
											building.create().show();
										} else {
											Toast.makeText(getContext(), "Password needs to have at least 6 characters with 1 uppercase, " +
													"1 lowercase and 1 digit. Please try again", Toast.LENGTH_SHORT).show();
										}
									} else {
										try {
											throw Objects.requireNonNull(task.getException());
										} catch (FirebaseAuthInvalidCredentialsException malformedEmail) {
											Toast.makeText(getActivity(), "Your email is invalid",
													Toast.LENGTH_SHORT).show();
										} catch (FirebaseAuthUserCollisionException existEmail) {
											Toast.makeText(getActivity(), "This is email was already registered",
													Toast.LENGTH_SHORT).show();
										} catch (Exception e) {
											e.printStackTrace();
											Toast.makeText(getActivity(), "Error occurred. Please try again",
													Toast.LENGTH_SHORT).show();
										}
									}
								}
							});

						} else if (clear == 2) {
							Toast.makeText(getContext(), "Please reconfirm your password", Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(getContext(), "Please fill in all fields or some field contains all spaces", Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
				requestCode == REQUEST_IMAGE && getActivity() != null) {
			MethodHelpers.startImageActivity(getActivity(), REQUEST_IMAGE);
		}
	}
}
