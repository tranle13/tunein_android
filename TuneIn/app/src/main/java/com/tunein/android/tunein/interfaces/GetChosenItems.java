package com.tunein.android.tunein.interfaces;

import java.util.ArrayList;

public interface GetChosenItems {
	void getChosenItems(ArrayList<String> items);
}
