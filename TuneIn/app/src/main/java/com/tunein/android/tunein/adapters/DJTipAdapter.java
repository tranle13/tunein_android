package com.tunein.android.tunein.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tunein.android.tunein.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

public class DJTipAdapter extends BaseAdapter {

	// Variables
	private ArrayList<Double> tipAndTime;
	private Context mContext;
	private int BASE_ID = 0x0100;

	// Initializer
	public DJTipAdapter(Context _context, ArrayList<Double> tips) {
		mContext = _context;
		tipAndTime = tips;
	}

	@Override
	public int getCount() {
		if (tipAndTime != null) {
			return tipAndTime.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if (tipAndTime != null && 0 <= position || position < Objects.requireNonNull(tipAndTime).size()) {
			return tipAndTime.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return BASE_ID + position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh;

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_dj_tip_listrow, parent,
					false);
			vh = new ViewHolder(convertView);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder)convertView.getTag();
		}

		Double amount = tipAndTime.get(position);
		if (amount != null) {
			String tip = "$"+String.format(Locale.US, "%.2f", amount);
			vh.textView_TipAmount.setText(tip);
		}

		return convertView;
	}

	// Recycle view pattern
	static class ViewHolder {
		final TextView textView_TipAmount;
		final TextView textView_Time;

		ViewHolder(View layout) {
			textView_TipAmount = layout.findViewById(R.id.textView_TipAmount);
			textView_Time = layout.findViewById(R.id.textView_Time);
		}
	}
}
