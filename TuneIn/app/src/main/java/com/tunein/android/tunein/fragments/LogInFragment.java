package com.tunein.android.tunein.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.helpers.MethodHelpers;
import com.tunein.android.tunein.interfaces.CallResetPass;
import com.tunein.android.tunein.interfaces.ChangeActivity;
import com.tunein.android.tunein.interfaces.ChangeFragment;

import java.io.FileOutputStream;
import java.util.Objects;

public class LogInFragment extends Fragment {

	// TODO: Variables
	private String TAG = "HELLO";
	private FirebaseAuth mAuth;
	private EditText editText_Email;
	private EditText editText_Password;
	private TextView textview_ForgotPassword;
	private Button button_LogIn;
	private Button button_Register;
	private ChangeFragment mListener;
	private ChangeActivity mListenerActivity;
	private CallResetPass mListenerPass;

	// Function to create new instance for the fragment
	public static LogInFragment newInstance() {

		Bundle args = new Bundle();

		LogInFragment fragment = new LogInFragment();
		fragment.setArguments(args);
		return fragment;
	}

	// Initialize FirebaseAuth
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAuth = FirebaseAuth.getInstance();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof ChangeFragment && context instanceof  ChangeActivity && context instanceof CallResetPass) {
			mListener = (ChangeFragment)context;
			mListenerActivity = (ChangeActivity)context;
			mListenerPass = (CallResetPass)context;
		}
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_main, container,false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getContext() != null) {
			editText_Email = getView().findViewById(R.id.editText_Email);
			editText_Password = getView().findViewById(R.id.editText_Password);
			textview_ForgotPassword = getView().findViewById(R.id.textview_ForgotPassword);
			button_LogIn = getView().findViewById(R.id.button_LogIn);
			button_Register = getView().findViewById(R.id.button_Register);

			button_LogIn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (MethodHelpers.isConnected(getContext())) {
						String emailStr = editText_Email.getText().toString();
						String passwordStr = editText_Password.getText().toString();

						int clear = MethodHelpers.checkIfTextsEmptyOrSpaced(emailStr, passwordStr, "", "",  0);
						if (clear == 0) {
							signIn(emailStr, passwordStr);
						} else {
							Toast.makeText(getContext(), "Please fill in all fields", Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
					}
				}
			});

			button_Register.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mListenerActivity != null) {
						mListenerActivity.changeActivity(false);
					}
				}
			});

			textview_ForgotPassword.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mListenerPass != null) {
						mListenerPass.callResetPass(editText_Email.getText().toString());
					}
				}
			});
		}
	}

	// Function to sign in existing users
	private void signIn(final String email, final String password) {
		if (mAuth != null && getActivity() != null) {
			mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(getActivity(),
					new OnCompleteListener<AuthResult>() {
						@Override
						public void onComplete(@NonNull Task<AuthResult> task) {
							if (task.isSuccessful()) {
								button_LogIn.setEnabled(false);

								SharedPreferences.Editor editor = getActivity().getSharedPreferences("CREDENTIALS", Context.MODE_PRIVATE).edit();
								editor.putString("email", email);
								editor.putString("password", password);
								editor.apply();

								Toast.makeText(getActivity(), "Successfully logged in!", Toast.LENGTH_SHORT).show();
								if (mListenerActivity != null && getContext() != null) {
									try {
										FileOutputStream fos = getContext().openFileOutput("tunein.txt", Context.MODE_PRIVATE);
										String signedIn = "Signed in";
										fos.write(signedIn.getBytes());
										fos.close();

										mListenerActivity.changeActivity(true);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							} else {
								try {
									throw Objects.requireNonNull(task.getException());
								} catch (FirebaseAuthInvalidUserException noEmail) {
									Toast.makeText(getActivity(), "Your email is not registered",
											Toast.LENGTH_SHORT).show();
								} catch (FirebaseAuthInvalidCredentialsException wrongPass) {
									Toast.makeText(getActivity(), "Your password or your email is incorrect",
											Toast.LENGTH_SHORT).show();
								} catch (Exception e) {
									e.printStackTrace();
									Toast.makeText(getActivity(), "Error occurred. Please try again",
											Toast.LENGTH_SHORT).show();
								}
							}
						}
					});
		}
	}
}
