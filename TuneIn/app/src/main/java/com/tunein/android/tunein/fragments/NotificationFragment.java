package com.tunein.android.tunein.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.adapters.NotificationPageAdapter;

public class NotificationFragment extends Fragment {

	private static boolean isDJ;

	public static NotificationFragment newInstance(boolean _isDJ) {
		isDJ = _isDJ;
		return new NotificationFragment();
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_notifications, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getActivity() != null) {
			TabLayout tabLayout = getView().findViewById(R.id.tabLayout);

			tabLayout.addTab(tabLayout.newTab().setText("Financial"));
			tabLayout.addTab(tabLayout.newTab().setText("Others"));

			tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

			final ViewPager viewPager = getView().findViewById(R.id.viewPager);
			final NotificationPageAdapter adapter = new NotificationPageAdapter(getActivity().getSupportFragmentManager(), isDJ);
			viewPager.setAdapter(adapter);
			viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
		}
	}
}
