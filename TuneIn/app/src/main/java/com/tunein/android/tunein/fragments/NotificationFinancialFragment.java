package com.tunein.android.tunein.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tunein.android.tunein.R;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

public class NotificationFinancialFragment extends ListFragment {

	// Variables
	private static boolean isDJ;
	private FirebaseAuth mAuth;
	private DatabaseReference mRef;
	private static final String TAG = "NotificationFinanFrag";
	private ArrayList<String> notifications = new ArrayList<>();

	// Initializer
	public static NotificationFinancialFragment newInstance(boolean _isDJ) {
		isDJ = _isDJ;
		return new NotificationFinancialFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_notifications_financial, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAuth = FirebaseAuth.getInstance();
		mRef = FirebaseDatabase.getInstance().getReference();

		String childName = "regular_users";
		if (isDJ) {
			childName = "dj_users";
		}

		if (getView() != null && getContext() != null && mAuth.getUid() != null) {
			mRef.child(childName).child(mAuth.getUid()).child("notifications").child("tips").addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					if (isDJ) {
						for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
							for (DataSnapshot subSnap: snapshot.getChildren()) {
								if (subSnap.getValue() != null) {
									notifications.add(subSnap.getValue().toString());
								}
							}
						}

						ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),
								android.R.layout.simple_list_item_1, notifications);
						setListAdapter(adapter);
					} else {
						for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
							if (snapshot.getKey() != null) {
								Log.i(TAG, "Snapshot");
								mRef.child("dj_users").child(snapshot.getKey()).child("fullname").addValueEventListener(new ValueEventListener() {
									@Override
									public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
										if (dataSnapshot1.getValue() != null) {
											String name = dataSnapshot1.getValue().toString();

											for (DataSnapshot subSnap: snapshot.getChildren()) {
												if (subSnap.getValue() != null) {
													Double amount = ((Long) subSnap.getValue()).doubleValue();
													String noti = "You tipped DJ " + name + " $" + String.format(Locale.US, "%.2f", amount);
													notifications.add(noti);

													ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),
															android.R.layout.simple_list_item_1, notifications);
													setListAdapter(adapter);
												}
											}

										}
									}

									@Override
									public void onCancelled(@NonNull DatabaseError databaseError) {
										Log.w(TAG, "onCancelled: ", databaseError.toException());
									}
								});
							}
						}
					}
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.w(TAG, "onCancelled: ", databaseError.toException());
				}
			});
		}
	}
}
