package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.DJInfoFragment;

public class DJInfoActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_djinfo);

		Intent startIntent = getIntent();

		if (startIntent != null && startIntent.getStringExtra("uid") != null) {
			getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_11,
					DJInfoFragment.newInstance(startIntent.getStringExtra("uid"))).commit();
		}
	}
}
