package com.tunein.android.tunein.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.interfaces.GetChosenItems;

import java.util.ArrayList;
import java.util.Objects;

public class GenreListAdapter extends BaseAdapter {

	private ArrayList<String> selectedGenre;
	private final ArrayList<String> genres;
	private final Context mContext;
	private static final int BASE_ID = 0x0011;
	private GetChosenItems mListener;

	// Constructor
	public GenreListAdapter(ArrayList<String> _genres, Context _context) {
		genres = _genres;
		mContext = _context;
		selectedGenre = new ArrayList<>();

		if (_context instanceof GetChosenItems) {
			mListener = (GetChosenItems) _context;
		}
	}

	@Override
	public int getCount() {
		if (genres != null) {
			return genres.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if (genres != null && 0 <= position || position < Objects.requireNonNull(genres).size()) {
			return genres.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position + BASE_ID;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder vh;

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_genre_listrow, parent,
					false);
			vh = new ViewHolder(convertView);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder)convertView.getTag();
		}

		if (genres.get(position) != null) {
			vh.textView_Genre.setText(genres.get(position));
			vh.checkBox_GenreCheck.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (selectedGenre.contains(genres.get(position))) {
						selectedGenre.remove(genres.get(position));
					} else {
						selectedGenre.add(genres.get(position));
					}

					if (mListener != null) {
						mListener.getChosenItems(selectedGenre);
					}
				}
			});
		}

		return convertView;
	}

	// Recycle view pattern
	static class ViewHolder {
		final CheckBox checkBox_GenreCheck;
		TextView textView_Genre;
		ViewHolder(View layout) {
			checkBox_GenreCheck = layout.findViewById(R.id.checkBox_GenreCheck);
			textView_Genre = layout.findViewById(R.id.textView_Genre);
		}
	}
}
