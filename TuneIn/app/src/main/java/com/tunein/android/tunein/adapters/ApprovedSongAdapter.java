package com.tunein.android.tunein.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.custom_objects.Track;

import java.util.List;

public class ApprovedSongAdapter extends ArrayAdapter<Track> {

	private List<Track> approvedTracks;
	private Context hostContext;

	public ApprovedSongAdapter(@NonNull Context context, @NonNull List<Track> objects) {
		super(context, 0, objects);
		approvedTracks = objects;
		hostContext = context;
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

		// Check if an existing view is being reused, otherwise inflate the view
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_2, parent, false);
		}

		TextView text1 = convertView.findViewById(android.R.id.text1);
		TextView text2 = convertView.findViewById(android.R.id.text2);
		text1.setTextColor(hostContext.getResources().getColor(R.color.colorAccent, null));
		text2.setTextColor(hostContext.getResources().getColor(R.color.gray, null));

		text1.setText(approvedTracks.get(position).getName());
		text2.setText(approvedTracks.get(position).getArtist());

		return convertView;
	}
}
