package com.tunein.android.tunein.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.helpers.MethodHelpers;
import com.tunein.android.tunein.interfaces.CallRespectiveActivity;
import com.tunein.android.tunein.interfaces.SignOut;

import java.util.Objects;

public class ProfileFragment extends Fragment implements View.OnClickListener {

	// Member variables
	private static Boolean isDJ;
	private FirebaseAuth mAuth;
	private SignOut mListenerSignOut;
	private CallRespectiveActivity mListenerCallActivity;
	private DatabaseReference mRef;

	public static ProfileFragment newInstance(Boolean isDJOrNot) {

		Bundle args = new Bundle();

		isDJ = isDJOrNot;

		ProfileFragment fragment = new ProfileFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof SignOut && context instanceof CallRespectiveActivity) {
			mListenerSignOut = (SignOut)context;
			mListenerCallActivity = (CallRespectiveActivity)context;
		}
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		mAuth = FirebaseAuth.getInstance();
		mRef = FirebaseDatabase.getInstance().getReference();
	}

	// Inflate the right layout to the view
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if (isDJ) {
			return inflater.inflate(R.layout.fragment_profile_dj, container, false);
		} else {
			return inflater.inflate(R.layout.fragment_profile_regular, container, false);
		}
	}

	// Function to get UI components and assign action to them
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && mAuth.getUid() != null && getContext() != null) {
			final ImageView imageView_ProfileImage = getView().findViewById(R.id.imageView_ProfileImage);
			final TextView textView_ProfileName = getView().findViewById(R.id.textView_ProfileName);
			final TextView textView_ProfileEmail = getView().findViewById(R.id.textView_ProfileEmail);
			final Button button_EditProfile = getView().findViewById(R.id.button_EditProfile);
			final Button button_Notifications = getView().findViewById(R.id.button_Notifications);
			final Button button_Follow = getView().findViewById(R.id.button_Follow);

			button_EditProfile.setOnClickListener(this);
			button_Notifications.setOnClickListener(this);
			button_Follow.setOnClickListener(this);

			if (isDJ) {
				mRef.child("dj_users").child(mAuth.getUid()).addValueEventListener(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						textView_ProfileName.setText((String)dataSnapshot.child("fullname").getValue());
						textView_ProfileEmail.setText((String)dataSnapshot.child("email").getValue());
						String url = (String)dataSnapshot.child("image").getValue();

						if (getContext() != null) {
							Glide.with(getContext()).load(url).into(imageView_ProfileImage);
						}
					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) { }
				});

				final Button button_DataVisualization = getView().findViewById(R.id.button_TipChart);
				final Button button_Genre = getView().findViewById(R.id.button_Genres);
				button_DataVisualization.setOnClickListener(this);
				button_Genre.setOnClickListener(this);

			} else {
				mRef.child("regular_users").child(mAuth.getUid()).addValueEventListener(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						textView_ProfileName.setText((String)dataSnapshot.child("fullname").getValue());
						textView_ProfileEmail.setText((String)dataSnapshot.child("email").getValue());
						String url = (String)dataSnapshot.child("image").getValue();

						if (getContext() != null) {
							Glide.with(getContext()).load(url).into(imageView_ProfileImage);
						}
					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) { }
				});
			}
		}
	}

	// Call the right fragment respective to the button
	@Override
	public void onClick(View v) {
		if (mListenerCallActivity != null) {
			mListenerCallActivity.callRespectiveActivity(v.getId(), null);
		}
	}

	// Inflate the menu layout
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_logout, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	// Code to delete internal file that keeps user signed in as well as sign out from FirebaseAuth
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_btn_logout && getActivity() != null && mAuth != null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Log Out");
			builder.setMessage("Are you sure you want to log out?");
			builder.setNegativeButton("Cancel", null);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (mListenerSignOut != null) {
						Objects.requireNonNull(getActivity()).deleteFile("tunein.txt");
						mAuth.signOut();
						mListenerSignOut.signOut();

						getActivity().getSharedPreferences("CREDENTIALS", Context.MODE_PRIVATE).edit().clear().apply();
					}
				}
			});

			builder.create().show();

		}
		return super.onOptionsItemSelected(item);
	}
}
