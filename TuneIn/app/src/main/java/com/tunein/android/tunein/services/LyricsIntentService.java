package com.tunein.android.tunein.services;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import com.tunein.android.tunein.activities.HomeActivity;
import com.tunein.android.tunein.custom_objects.Track;
import com.tunein.android.tunein.fragments.LyricsFragment;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

public class LyricsIntentService extends IntentService {

	public LyricsIntentService() {
		super("LyricsIntentService");
	}

	@Override
	protected void onHandleIntent(@Nullable Intent intent) {
		ResultReceiver receiver = null;
		Track chosenTrack = new Track();
		String lyrics;

		// Get ResultReceiver from intent
		try {
			if (intent != null) {
				receiver = intent.getParcelableExtra(HomeActivity.EXTRA_RESULT_RECEIVER);
				chosenTrack = (Track) intent.getSerializableExtra(LyricsFragment.EXTRA_TRACK);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		URL url;
		HttpURLConnection connection = null;
		InputStream stream;

		String webAddress = "https://orion.apiseeds.com/api/music/lyric/"+chosenTrack.getArtist()+"/"+chosenTrack.getName()
				+"?apikey=yPSQuoZUV9NCxSjfjBBKbBKynIiU4KBMD9LwifGKjxmjAkuIL52HouzvuuKQlev1";

		// Open connection
		try {
			url = new URL(webAddress);
			connection = (HttpURLConnection)url.openConnection();
			connection.connect();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Open stream and close after done getting data
		try {
			stream = Objects.requireNonNull(connection).getInputStream();
			String result = IOUtils.toString(stream, "UTF-8");

			JSONObject outerObj = new JSONObject(result);
			JSONObject innerObj = outerObj.getJSONObject("result");
			JSONObject lyricsObj = innerObj.getJSONObject("track");
			lyrics = lyricsObj.getString("text");

			if (receiver != null) {
				Bundle data = new Bundle();
				data.putString("lyrics", lyrics);
				receiver.send(Activity.RESULT_OK, data);
			}

			connection.disconnect();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
