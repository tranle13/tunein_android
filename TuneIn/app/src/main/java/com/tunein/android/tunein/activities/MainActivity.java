package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.LogInFragment;
import com.tunein.android.tunein.fragments.RegisterChoiceFragment;
import com.tunein.android.tunein.fragments.ResetPasswordFragment;
import com.tunein.android.tunein.interfaces.CallResetPass;
import com.tunein.android.tunein.interfaces.ChangeActivity;
import com.tunein.android.tunein.interfaces.ChangeFragment;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;

public class MainActivity extends AppCompatActivity implements ChangeFragment, ChangeActivity, CallResetPass {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		try {
			FileInputStream fis = openFileInput("tunein.txt");
			String term = IOUtils.toString(fis, "UTF-8");
			fis.close();

			if (term.equals("Signed in")) {
				startNewActivity();
				return;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Code to replace container with appropriate fragment
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_1,
					LogInFragment.newInstance()).commit();
		}
	}

	@Override
	public void changeScreen() {
		getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer_1, RegisterChoiceFragment.newInstance()).commit();
	}

	// Function to start new activity
	private void startNewActivity() {
		Intent nextIntent = new Intent(this, HomeActivity.class);
		startActivity(nextIntent);
		finish();
	}

	@Override
	public void changeActivity(Boolean isHome) {
		if (isHome) {
			startNewActivity();
		} else {
			Intent nextIntent = new Intent(this, ChooseRegistrationActivity.class);
			startActivity(nextIntent);
		}
	}

	@Override
	public void callResetPass(String email) {
		Intent nextIntent = new Intent(this, ResetPasswordActivity.class);
		nextIntent.putExtra("email", email);
		startActivity(nextIntent);
	}
}
