package com.tunein.android.tunein.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.adapters.GenreListAdapter;
import com.tunein.android.tunein.interfaces.ChangeActivity;
import com.tunein.android.tunein.interfaces.DismissActivity;

import java.util.ArrayList;
import java.util.Arrays;

public class DJEditGenresFragment extends Fragment {

	private ArrayList<String> genres = new ArrayList<>();
	public static ArrayList<String> chosenGenres = new ArrayList<>();
	private FirebaseAuth mAuth;
	private DatabaseReference mRef;
	private static final String TAG = "DJViewGenresFragment";
	private DismissActivity mListener;
	private ChangeActivity mListenerActivity;
	private ConstraintLayout emptyState;
	private ListView listView_Genre;
	public static ArrayList<String> newlyAddedGenres = new ArrayList<>();

	public static DJEditGenresFragment newInstance() {
		return new DJEditGenresFragment();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if (context instanceof DismissActivity && context instanceof ChangeActivity) {
			mListener = (DismissActivity)context;
			mListenerActivity = (ChangeActivity) context;
		}
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onResume() {
		super.onResume();

		for (String musicTaste: newlyAddedGenres) {
			if (!(genres.contains(musicTaste))) {
				genres.add(musicTaste);
			}
		}

		if (genres.size() > 0) {
			emptyState.setVisibility(View.GONE);
			GenreListAdapter adapter = new GenreListAdapter(genres, getContext());
			listView_Genre.setAdapter(adapter);
		} else {
			emptyState.setVisibility(View.VISIBLE);
		}
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_profile_dj_edit_genres, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAuth = FirebaseAuth.getInstance();
		mRef = FirebaseDatabase.getInstance().getReference().child("dj_users");

		if (getView() != null && mAuth.getUid() != null && getContext() != null) {
			listView_Genre = getView().findViewById(R.id.listView_Genres);
			final Button button_Save = getView().findViewById(R.id.button_SaveChanges);
			emptyState = getView().findViewById(R.id.empty_state);

			mRef.child(mAuth.getUid()).child("genres").addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					if (dataSnapshot.getValue() != null) {
						genres.clear();
						String[] allGenres = dataSnapshot.getValue().toString().split(",");
						for (String theGenre: allGenres) {
							genres.add(theGenre.trim());
						}

						if (genres.size() > 0) {
							emptyState.setVisibility(View.GONE);
							GenreListAdapter adapter = new GenreListAdapter(genres, getContext());
							listView_Genre.setAdapter(adapter);
						} else {
							emptyState.setVisibility(View.VISIBLE);
						}
					}
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.w(TAG, "onCancelled: ", databaseError.toException());
				}
			});

			button_Save.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mListener != null) {
						StringBuilder finalGenre = new StringBuilder();
						for (int i = 0; i < genres.size(); i++) {
							if (i == genres.size() - 1) {
								finalGenre.append(genres.get(i));
							} else {
								finalGenre.append(genres.get(i)).append(", ");
							}
						}
						mRef.child(mAuth.getUid()).child("genres").setValue(finalGenre.toString());
						mListener.dismissActivity();
					}
				}
			});
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_detail_edit, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_btn_delete) {

			AlertDialog.Builder build = new AlertDialog.Builder(getContext());

			if (chosenGenres.size() > 0) {
				build.setTitle("Delete Genres");
				build.setMessage("Are you sure you want to delete these genres?");
				build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						for (String genre : chosenGenres) {
							genres.remove(genre);
						}

						if (genres.size() > 0) {
							emptyState.setVisibility(View.GONE);
							GenreListAdapter adapter = new GenreListAdapter(genres, getContext());
							listView_Genre.setAdapter(adapter);
						} else {
							emptyState.setVisibility(View.VISIBLE);
						}
					}
				});

				build.setNegativeButton("Cancel", null);
			} else {
				build.setMessage("Please select one or more genres to delete");
				build.setNegativeButton("OK", null);
			}
			build.create().show();
		} else {
			if (mListenerActivity != null) {
				mListenerActivity.changeActivity(null);
			}
		}
		return super.onOptionsItemSelected(item);
	}
}
