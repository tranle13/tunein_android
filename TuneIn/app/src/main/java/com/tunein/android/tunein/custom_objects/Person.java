package com.tunein.android.tunein.custom_objects;

import android.graphics.Bitmap;

public class Person {

	// Member variables
	private final Boolean isDJ;
	private final String email;
	private final String fullName;
	private final String uid;
	private final String image;

	public Person() {
		uid = "No uid";
		email = "No email";
		fullName = "No name";
		isDJ = false;
		image = null;
	}

	// Constructor
	public Person(String _fullname, String _uid, String _email, Boolean _isDJ, String _image) {
		uid = _uid;
		email = _email;
		fullName = _fullname;
		isDJ = _isDJ;
		image = _image;
	}

	// Getters

	public String getEmail() {
		return email;
	}

	public Boolean getDJ() {
		return isDJ;
	}

	public String getFullName() {
		return fullName;
	}

	public String getUid() {
		return uid;
	}

	public String getImage() {
		return image;
	}
}
