package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.DataVisualizationFragment;

public class DataVisualizationActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_data_visualization);

		Intent startIntent = getIntent();

		if (startIntent != null && getSupportActionBar() != null) {
			switch (startIntent.getIntExtra("chart", 0)) {
				case 0:
					getSupportActionBar().setTitle("Weekly Tip Visualization");
					break;
				case 1:
					getSupportActionBar().setTitle("Monthly Tip Visualization");
					break;
				default:
					getSupportActionBar().setTitle("Yearly Tip Visualization");
					break;
			}
			getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_13,
					DataVisualizationFragment.newInstance(startIntent.getIntExtra("chart", 0))).commit();
		}
	}
}
