package com.tunein.android.tunein.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.interfaces.DismissActivity;

import java.util.Objects;

public class ResetPasswordFragment extends Fragment {

	// Member variables
	private static String email;
	private FirebaseAuth mAuth;
	private DismissActivity mListenerDismiss;

	// Initializer
	public static ResetPasswordFragment newInstance(String _email) {
		email = _email;
		return new ResetPasswordFragment();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if (context instanceof DismissActivity) {
			mListenerDismiss = (DismissActivity) context;
		}
	}

	// Initialize fragment's layout
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_forgot_password, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAuth = FirebaseAuth.getInstance();

		if (getView() != null && getContext() != null) {
			final EditText editText_Email = getView().findViewById(R.id.editText_Email);
			editText_Email.setText(email);
			final Button button_SendEmail = getView().findViewById(R.id.button_SendEmail);

			button_SendEmail.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mAuth.sendPasswordResetEmail(editText_Email.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
						@Override
						public void onComplete(@NonNull Task<Void> task) {
							if (task.isSuccessful()) {
								Toast.makeText(getContext(), "Reset password email is sent", Toast.LENGTH_SHORT).show();
								if (mListenerDismiss != null) {
									mListenerDismiss.dismissActivity();
								}
							} else {

								try {
									throw Objects.requireNonNull(task.getException());
								} catch (FirebaseAuthInvalidCredentialsException malformedEmail) {
									Toast.makeText(getActivity(), "Your email is malformed",
											Toast.LENGTH_SHORT).show();
								} catch (FirebaseAuthUserCollisionException existEmail) {
									Toast.makeText(getActivity(), "This is email was already registered",
											Toast.LENGTH_SHORT).show();
								} catch (Exception e) {
									e.printStackTrace();
									Toast.makeText(getActivity(), "Email hasn't been registered or error occurred. Please try again",
											Toast.LENGTH_SHORT).show();
								}
							}
						}
					});
				}
			});
		}
	}
}
