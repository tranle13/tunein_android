package com.tunein.android.tunein.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tunein.android.tunein.R;

import java.util.ArrayList;

public class DJInfoFragment extends Fragment {

	private static String djUid;
	private FirebaseAuth mAuth;
	private DatabaseReference mRef;
	private static final String TAG = "DJInfoFragment";
	private String djName;

	public static DJInfoFragment newInstance(String _djUid) {
		djUid = _djUid;
		return new DJInfoFragment();
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_dj_information, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAuth = FirebaseAuth.getInstance();
		mRef = FirebaseDatabase.getInstance().getReference();

		if (getView() != null && getContext() != null && mAuth.getUid() != null) {
			final ImageView imageView_Image = getView().findViewById(R.id.imageView_DJImage);
			final TextView textView_Name = getView().findViewById(R.id.textView_DJName);
			final TextView textView_Email = getView().findViewById(R.id.textView_DJEmail);
			final ListView listView_Genres = getView().findViewById(R.id.listview_Genres);
			final Button button_Follow = getView().findViewById(R.id.button_Follow);
			final ConstraintLayout emptyStateGenre = getView().findViewById(R.id.empty_state);

			mRef.child("regular_users").child(mAuth.getUid()).child("followed_djs").addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
						if (snapshot.getKey() != null && snapshot.getKey().equals(djUid)) {
							button_Follow.setText(R.string.button_Unfollow);
							break;
						}
					}
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.i(TAG, "onCancelled: " + databaseError.getMessage());
				}
			});

			mRef.child("dj_users").child(djUid).addListenerForSingleValueEvent(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					djName = (String)dataSnapshot.child("fullname").getValue();
					textView_Name.setText(djName);
					textView_Email.setText((String)dataSnapshot.child("email").getValue());
					Glide.with(getContext()).load((String)dataSnapshot.child("image").getValue()).into(imageView_Image);
					String genres = (String)dataSnapshot.child("genre").getValue();
					ArrayList<String> separateGenre = new ArrayList<>();
					if (genres != null) {
						String[] allGenres = genres.split(",");
						for (String genre: allGenres) {
							separateGenre.add(genre.trim());
						}
					}

					if (separateGenre.size() > 0) {
						emptyStateGenre.setVisibility(View.GONE);
						ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, separateGenre);
						listView_Genres.setAdapter(adapter);
					} else {
						emptyStateGenre.setVisibility(View.VISIBLE);
					}
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.w(TAG, "onCancelled: ", databaseError.toException());
				}
			});

			button_Follow.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (button_Follow.getText().toString().toLowerCase().equals("follow")) {
						mRef.child("dj_users").child(djUid).child("followers").child(mAuth.getUid()).setValue("following");
						mRef.child("regular_users").child(mAuth.getUid()).child("followed_djs").child(djUid).setValue(djName);
						button_Follow.setText(R.string.button_Unfollow);
					} else {
						mRef.child("dj_users").child(djUid).child("followers").child(mAuth.getUid()).removeValue();
						mRef.child("regular_users").child(mAuth.getUid()).child("followed_djs").child(djUid).removeValue();
						button_Follow.setText(R.string.button_Follow);
					}
				}
			});
		}
	}
}
