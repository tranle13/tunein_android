package com.tunein.android.tunein.custom_objects;

import java.io.Serializable;

public class Track implements Serializable {

	// Member variables
	private final String name;
	private final String artist;
	private final String url;

	public Track() {
		name = "No name";
		artist = "No artist";
		url = "No cover";
	}

	// Constructor
	public Track(String _name, String _artist, String _cover) {
		name = _name;
		artist = _artist;
		url = _cover;
	}

	// Getters
	public String getName() {
		return name;
	}

	public String getArtist() {
		return artist;
	}

	public String getCover() {
		return url;
	}
}
