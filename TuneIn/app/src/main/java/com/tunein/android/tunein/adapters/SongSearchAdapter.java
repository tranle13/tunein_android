package com.tunein.android.tunein.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.custom_objects.Track;
import com.tunein.android.tunein.interfaces.RequestSong;

import java.util.ArrayList;

public class SongSearchAdapter extends BaseAdapter {

	// Member variables
	private ArrayList<Track> results;
	private final Context mContext;
	private static final int BASE_ID = 0x0111;
	private RequestSong mListener;

	// Initializer
	public SongSearchAdapter(Context _context, ArrayList<Track> _results) {
		results = _results;
		mContext = _context;
		if (_context instanceof RequestSong) {
			mListener = (RequestSong)_context;
		}
	}

	@Override
	public int getCount() {
		if (results != null) {
			return results.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if (results != null && 0 <= position && results.size() > position) {
			return results.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return BASE_ID + position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder vh;

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_search_listrow, parent,
					false);
			vh = new ViewHolder(convertView);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder)convertView.getTag();
		}

		if (results.get(position) != null) {
			vh.textView_Title.setText(results.get(position).getName());
			vh.textView_Artist.setText(results.get(position).getArtist());
			Glide.with(mContext).load(results.get(position).getCover()).into(vh.imageview_Image);
			vh.button_Request.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mListener != null) {
						mListener.requestSong(results.get(position));
					}
				}
			});
		}

		convertView.setClickable(false);

		return convertView;
	}

	// Recycle view pattern
	static class ViewHolder {
		final ImageView imageview_Image;
		final TextView textView_Title;
		final TextView textView_Artist;
		final ImageButton button_Request;

		ViewHolder(View layout) {
			imageview_Image = layout.findViewById(R.id.imageView_TrackCover);
			textView_Title = layout.findViewById(R.id.textView_Title);
			textView_Artist = layout.findViewById(R.id.textView_Artist);
			button_Request = layout.findViewById(R.id.button_Request);
		}
	}
}
