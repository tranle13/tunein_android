package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.RegularLiveDJ;

public class LiveDJActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_live_dj);

		Intent startIntent = getIntent();

		if (startIntent != null && startIntent.getStringExtra("uid") != null) {
			getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_8,
					RegularLiveDJ.newInstance(startIntent.getStringExtra("uid"))).commit();
		}
	}
}
