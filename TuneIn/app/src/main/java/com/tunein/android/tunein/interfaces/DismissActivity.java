package com.tunein.android.tunein.interfaces;

public interface DismissActivity {
	void dismissActivity();
}
