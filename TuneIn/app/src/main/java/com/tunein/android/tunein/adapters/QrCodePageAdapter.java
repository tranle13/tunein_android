package com.tunein.android.tunein.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.tunein.android.tunein.fragments.AppQrCodeFragment;
import com.tunein.android.tunein.fragments.DJQrCodeFragment;
import com.tunein.android.tunein.fragments.QrScannerFragment;

public class QrCodePageAdapter extends FragmentStatePagerAdapter {

	// Member variables
	private int numOfTabs;
	private Boolean isDJ;

	public QrCodePageAdapter(FragmentManager fm, int _numOfTabs, Boolean _isDJ) {
		super(fm);
		numOfTabs = _numOfTabs;
		isDJ = _isDJ;
	}

	@Override
	public Fragment getItem(int i) {
		switch (i) {
			case 0:
				return new AppQrCodeFragment();
			case 1:
				if (!isDJ) {
					return new QrScannerFragment();
				} else {
					return new DJQrCodeFragment();
				}
			default:
				Log.i("QrCodePageAdapter", "getItem: ");
		}
		return null;
	}

	@Override
	public int getCount() {
		return numOfTabs;
	}
}
