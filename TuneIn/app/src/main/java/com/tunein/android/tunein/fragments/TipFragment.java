package com.tunein.android.tunein.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.snapshot.DoubleNode;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.adapters.DJTipAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class TipFragment extends Fragment {

	// Variables
	private static Boolean isDJ;
	private Integer counter = 0;
	private FirebaseAuth mAuth;
	private DatabaseReference mRef;
	private static final String TAG = "TipFragment";
	private String currentLiveDjUid;

	// Initializer
	public static TipFragment newInstance(Boolean isDJOrNot) {

		Bundle args = new Bundle();

		isDJ = isDJOrNot;

		TipFragment fragment = new TipFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if (isDJ) {
			return inflater.inflate(R.layout.fragment_tip_dj, container, false);
		} else {
			return inflater.inflate(R.layout.fragment_tip_regular, container, false);
		}
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAuth = FirebaseAuth.getInstance();
		mRef = FirebaseDatabase.getInstance().getReference();

		if (getView() != null && getContext() != null && getActivity() != null && mAuth.getUid() != null) {
			if (isDJ) {
				final ListView listView_Tip = getView().findViewById(R.id.listView_Tips);
				final ConstraintLayout emptyState = getView().findViewById(R.id.empty_state);

				Date today = Calendar.getInstance().getTime();
				SimpleDateFormat mdformat = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
				String strDate = mdformat.format(today);
				final String[] dateTime = strDate.split("_");

				mRef.child("dj_users").child(Objects.requireNonNull(mAuth.getUid())).child("tips").addValueEventListener(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						ArrayList<Double> tips = new ArrayList<>();
						for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
							for (DataSnapshot subSnapshot: snapshot.getChildren()) {
								String[] savedDateTime = subSnapshot.getKey().split("_");
								if (dateTime[0].equals(savedDateTime[0]) && dateTime[1].equals(savedDateTime[1])
										&& dateTime[2].equals(savedDateTime[2])) {
									if (subSnapshot.getValue() != null) {
										tips.add(((Long)subSnapshot.getValue()).doubleValue());
									}
								}
							}
						}

						if (tips.size() > 0) {
							emptyState.setVisibility(View.GONE);
							DJTipAdapter adapter = new DJTipAdapter(getContext(), tips);
							listView_Tip.setAdapter(adapter);
						} else {
							emptyState.setVisibility(View.VISIBLE);
						}
					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
						Log.w(TAG, "onCancelled: ", databaseError.toException());
					}
				});

			} else {
				final ImageView imageView_DJImage = getView().findViewById(R.id.imageView_DJImage);
				final TextView textView_DJName = getView().findViewById(R.id.textView_DJName);
				final TextView textView_Tip = getView().findViewById(R.id.editText_TipAmount);
				final ImageButton button_Increase = getView().findViewById(R.id.button_Increase);
				final ImageButton button_Decrease = getView().findViewById(R.id.button_Decrease);
				final Button button_Tip = getView().findViewById(R.id.button_Tip);
				final ConstraintLayout noLiveDJView = getView().findViewById(R.id.empty_state);

				mRef.child("regular_users").child(mAuth.getUid()).addValueEventListener(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						if (dataSnapshot.child("currentLiveDJ").getValue() != null) {
							noLiveDJView.setVisibility(View.GONE);
							String uid = dataSnapshot.child("currentLiveDJ").getValue().toString();
							currentLiveDjUid = uid;

							mRef.child("dj_users").child(uid).addValueEventListener(new ValueEventListener() {
								@Override
								public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
									String name = (String)dataSnapshot.child("fullname").getValue();
									String url = (String)dataSnapshot.child("image").getValue();

									if (name != null && url != null && getContext() != null) {
										textView_DJName.setText(name);
										Glide.with(getContext()).load(url).into(imageView_DJImage);
									}
								}

								@Override
								public void onCancelled(@NonNull DatabaseError databaseError) {
									Log.w(TAG, "onCancelled: ", databaseError.toException());
								}
							});
						} else {
							noLiveDJView.setVisibility(View.VISIBLE);
						}
					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
						Log.w(TAG, "onCancelled: ", databaseError.toException());
					}
				});

				String tipStr = "$"+counter;
				textView_Tip.setText(tipStr);

				if (counter == 0) {
					button_Tip.setEnabled(false);
				} else {
					button_Tip.setEnabled(true);
				}

				View.OnClickListener clickListener = new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (v.getId() == R.id.button_Increase) {
							counter++;
						} else {
							counter--;
						}

						String tip = "$"+counter;
						textView_Tip.setText(tip);

						if (counter == 0) {
							button_Tip.setEnabled(false);
						} else {
							button_Tip.setEnabled(true);
						}
					}
				};

				button_Increase.setOnClickListener(clickListener);
				button_Decrease.setOnClickListener(clickListener);

				button_Tip.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Date today = Calendar.getInstance().getTime();
						SimpleDateFormat mdformat = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
						String strDate = mdformat.format(today);
						mRef.child("dj_users").child(currentLiveDjUid).child("tips").child(mAuth.getUid()).child(strDate).setValue(counter);

						counter = 0;
						textView_Tip.setText("$0");
					}
				});
			}
		}
	}
}
