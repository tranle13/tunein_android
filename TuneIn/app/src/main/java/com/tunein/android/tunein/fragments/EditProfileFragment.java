package com.tunein.android.tunein.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.helpers.MethodHelpers;
import com.tunein.android.tunein.interfaces.CallCamera;
import com.tunein.android.tunein.interfaces.DismissActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Objects;

public class EditProfileFragment extends Fragment {

	private static Boolean isDJ;
	private DatabaseReference mRef;
	private FirebaseAuth mAuth;
	private EditText editText_Name;
	private EditText editText_Email;
	private String beforeEmail;
	private String beforeName;
	private ImageView imageView_Image;
	private CallCamera mListenerCam;
	public static File chosenImage;
	private DismissActivity mListenerDismiss;

	public static EditProfileFragment newInstance(boolean isDJOrNot) {

		Bundle args = new Bundle();

		isDJ = isDJOrNot;

		EditProfileFragment fragment = new EditProfileFragment();
		fragment.setArguments(args);
		return fragment;
	}

	// Function to check if context implements interface
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if (context instanceof CallCamera && context instanceof DismissActivity) {
			mListenerCam = (CallCamera)context;
			mListenerDismiss = (DismissActivity)context;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (chosenImage != null) {
			imageView_Image.setImageBitmap(MethodHelpers.scaleBitmap(BitmapFactory.decodeFile(chosenImage.getAbsolutePath())));
		}
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_edit_profile, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getContext() != null && MethodHelpers.isConnected(getContext())) {
			mAuth = FirebaseAuth.getInstance();

			if (getView() != null && mAuth.getUid() != null) {
				if (isDJ) {
					mRef = FirebaseDatabase.getInstance().getReference().child("dj_users").child(mAuth.getUid());
				} else {
					mRef = FirebaseDatabase.getInstance().getReference().child("regular_users").child(mAuth.getUid());
				}

				imageView_Image = getView().findViewById(R.id.imageView_Image);
				editText_Name = getView().findViewById(R.id.editText_Name);
				editText_Email = getView().findViewById(R.id.editText_Email);

				mRef.addListenerForSingleValueEvent(new ValueEventListener() {
					@Override
					public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
						beforeName = (String) dataSnapshot.child("fullname").getValue();
						beforeEmail = (String) dataSnapshot.child("email").getValue();

						editText_Email.setText(beforeEmail);
						editText_Name.setText(beforeName);

						String url = (String)dataSnapshot.child("image").getValue();
						Glide.with(Objects.requireNonNull(getContext())).load(url).into(imageView_Image);
					}

					@Override
					public void onCancelled(@NonNull DatabaseError databaseError) {
					}
				});

				imageView_Image.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						MethodHelpers.createGalleryCameraAlert(getActivity(), mListenerCam,
								DJRegisterFragment.REQUEST_IMAGE);
					}
				});
			}
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_save, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_btn_save) {
			if (getContext() != null && getActivity() != null && MethodHelpers.isConnected(getContext())
					&& getLayoutInflater() != null && mAuth.getUid() != null && mAuth.getCurrentUser() != null) {
				String message = MethodHelpers.checkEditText(editText_Email.getText().toString(), editText_Email.getText().toString(), editText_Name.getText().toString());
				if (message.isEmpty()) {
					if (!editText_Email.getText().toString().equals(beforeEmail) && mAuth.getCurrentUser() != null) {

						SharedPreferences prefs = getActivity().getSharedPreferences("CREDENTIALS", Context.MODE_PRIVATE);
						String password = prefs.getString("password", null);

						if (password != null) {
							// Get auth credentials from the user for re-authentication
							AuthCredential credential = EmailAuthProvider
									.getCredential(beforeEmail, password);

							// Prompt the user to re-provide their sign-in credentials
							mAuth.getCurrentUser().reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
								@Override
								public void onComplete(@NonNull Task<Void> task) {
									mAuth.getCurrentUser().updateEmail(editText_Email.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
										@Override
										public void onComplete(@NonNull Task<Void> task) {
											mRef.child("email").setValue(editText_Email.getText().toString());
										}
									});
								}
							});
						}
					}

					if (!editText_Name.getText().toString().equals(beforeName)) {
						mRef.child("fullname").setValue(editText_Name.getText().toString());
					}

					Bitmap profilePicture = ((BitmapDrawable)imageView_Image.getDrawable()).getBitmap();
					ByteArrayOutputStream stream = new ByteArrayOutputStream();

					profilePicture.compress(Bitmap.CompressFormat.JPEG, 100, stream);
					byte[] bitmapData = stream.toByteArray();


					// Create file metadata including the content type
					StorageMetadata metadata = new StorageMetadata.Builder()
							.setContentType("image/jpg")
							.build();

					StorageReference storageRef = FirebaseStorage.getInstance().getReference().child(mAuth.getUid());
					// Upload the file and metadata
					storageRef.child(mAuth.getUid()).putBytes(bitmapData, metadata).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
						@Override
						public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
							Toast.makeText(getContext(), "Saved changes successfully", Toast.LENGTH_SHORT).show();
							if (mListenerDismiss != null) {
								mListenerDismiss.dismissActivity();
							}
						}
					}).addOnFailureListener(new OnFailureListener() {
						@Override
						public void onFailure(@NonNull Exception e) {
							Toast.makeText(getContext(), "Error occurred. Please try again.", Toast.LENGTH_SHORT).show();
						}
					});

				} else {
					Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
				}


			}
		}
		return super.onOptionsItemSelected(item);
	}
}
