package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.custom_objects.Track;
import com.tunein.android.tunein.fragments.LyricsFragment;

import java.util.Objects;

public class SongLyricsActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_song_lyrics);

		Intent startIntent = getIntent();

		if (startIntent != null & Objects.requireNonNull(startIntent).getSerializableExtra(SearchSongActivity.EXTRA_CHOSEN_TRACK) != null) {
			Track chosenTrack = (Track) startIntent.getSerializableExtra(SearchSongActivity.EXTRA_CHOSEN_TRACK);
			getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_9,
					LyricsFragment.newInstance(chosenTrack)).commit();

			Objects.requireNonNull(getSupportActionBar()).setTitle(chosenTrack.getName() + " - " + chosenTrack.getArtist());
		}
	}
}
