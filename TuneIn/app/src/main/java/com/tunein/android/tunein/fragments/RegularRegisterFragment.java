package com.tunein.android.tunein.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.helpers.MethodHelpers;
import com.tunein.android.tunein.interfaces.ChangeActivity;

import java.util.Objects;
import java.util.regex.Pattern;

public class RegularRegisterFragment extends Fragment {

	// Variables
	private FirebaseAuth mAuth;
	private ChangeActivity mListener;

	public static RegularRegisterFragment newInstance() {
		return new RegularRegisterFragment();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAuth = FirebaseAuth.getInstance();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof ChangeActivity) {
			mListener = (ChangeActivity)context;
		}
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_regular_registration, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getContext() != null && getActivity() != null && mListener != null) {
			// Code to get UI components, their values and carry out respective action
			final EditText editText_Email = getView().findViewById(R.id.editText_Email);
			final EditText editText_FullName = getView().findViewById(R.id.editText_FullName);
			final EditText editText_Password = getView().findViewById(R.id.editText_Password);
			final EditText editText_ConfirmPass = getView().findViewById(R.id.editText_ConfirmPass);
			final Button button_Register = getView().findViewById(R.id.button_Register);
			final Switch switch_AndroidPay = getView().findViewById(R.id.switch_AndroidPay);

			switch_AndroidPay.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					AlertDialog.Builder building = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
					building.setTitle("Tune-In Would Like To Access Your Android Pay");
					building.setMessage("This allows user to tip the DJ faster and more convenient");

					building.setPositiveButton("OK", null);

					building.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							switch_AndroidPay.setChecked(false);
						}
					});
					building.create().show();
				}
			});

			button_Register.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (MethodHelpers.isConnected(getContext())) {
						final String emailStr = editText_Email.getText().toString();
						final String passwordStr = editText_Password.getText().toString();
						final String fullNameStr = editText_FullName.getText().toString();
						final String confirmPassStr = editText_ConfirmPass.getText().toString();

						int clear = MethodHelpers.checkIfTextsEmptyOrSpaced(emailStr, passwordStr, fullNameStr, confirmPassStr, 1);
						if (clear == 0) {
							final boolean isAtLeast6 = passwordStr.length() >= 6;
							Pattern regex = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$");
							final boolean containRequiredCharacters = regex.matcher(passwordStr).matches();

							if (isAtLeast6 && containRequiredCharacters) {
								Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.profile_image_default);
								MethodHelpers.createNewAccount(mAuth, getActivity(), mListener, getContext(),
										false, emailStr, passwordStr, "", fullNameStr, image, false, null);
							} else {
								Toast.makeText(getContext(), "Password needs to have at least 6 characters with 1 uppercase, " +
										"1 lowercase and 1 digit. Please try again", Toast.LENGTH_SHORT).show();
							}

						} else if (clear == 2) {
							Toast.makeText(getContext(), "Please reconfirm your password", Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(getContext(), "Please fill in all fields", Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
	}
}
