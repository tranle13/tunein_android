package com.tunein.android.tunein.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.interfaces.CallCamera;
import com.tunein.android.tunein.interfaces.ChangeActivity;
import com.tunein.android.tunein.interfaces.DismissActivity;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MethodHelpers {

	// Function to check if any of the EditTexts are empty
	public static int checkIfTextsEmptyOrSpaced(String email, String password, String fullname,
												String confirmPass, int signUpOrIn) {
		String finalEmail = email.replace(" ", "");
		String finalPass = password.replace(" ", "");
		String finalFullName = fullname.replace(" ", "");
		String finalConfirmPass = confirmPass.replace(" ", "");

		if (finalEmail.isEmpty() || finalPass.isEmpty()) {
			return 1;
		}

		if (signUpOrIn == 1) {
			if (finalFullName.isEmpty()
					|| finalConfirmPass.isEmpty()) {
				return 1;
			} else if (!confirmPass.equals(password)) {
				return 2;
			}
		}
		return 0;
	}

	// Function to check if the user has internet connection
	public static boolean isConnected(Context context) {
		ConnectivityManager mgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (mgr != null) {
			NetworkInfo info = mgr.getActiveNetworkInfo();

			if (info != null) {
				return info.isConnected();
			}
		}

		return false;
	}

	// Function to round corner bitmap before assigning it to ImageView
	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		final Paint paint = new Paint();
		Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		RectF rectF = new RectF(rect);

		paint.setAntiAlias(true);
		canvas.drawARGB(0,0,0,0);
		canvas.drawRoundRect(rectF, (float) (bitmap.getWidth()/2), (float) (bitmap.getWidth()/2), paint);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}

	// Function to get image from FirebaseStorage
	public static byte[] getImage(Task<byte[]> task, Resources resources) {
		byte[] image;
		if (task.isSuccessful()) {
			image = task.getResult();

		} else {
			Bitmap imageBmp = BitmapFactory.decodeResource(resources, R.drawable.profile_image_default);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			imageBmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			image = baos.toByteArray();
			imageBmp.recycle();
		}

		return image;
	}

	// Function to create new account on Firebase with email and password
	public static void createNewAccount(final FirebaseAuth mAuth, final FragmentActivity activity, final ChangeActivity mListener,
										final Context context, final Boolean isDJ,
										final String email, final String password, final String genre, final String fullName,
										final Bitmap image, final Boolean isEditProfile, final DismissActivity mListenerDismiss) {
		if (mAuth != null && activity != null) {
				mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
					@Override
					public void onComplete(@NonNull Task<AuthResult> task) {
						if (task.isSuccessful()) {
							SharedPreferences.Editor editor = activity.getSharedPreferences("CREDENTIALS", Context.MODE_PRIVATE).edit();
							editor.putString("email", email);
							editor.putString("password", password);
							editor.apply();

							Toast.makeText(activity, "Successfully signed up!", Toast.LENGTH_SHORT).show();
							if (mListener != null && context != null) {
								try {
									FileOutputStream fos = context.openFileOutput("tunein.txt", Context.MODE_PRIVATE);
									String signedIn = "Signed in";
									fos.write(signedIn.getBytes());
									fos.close();
								} catch (Exception e) {
									e.printStackTrace();
								}

								mListener.changeActivity(true);
							}

							FirebaseUser mUser = mAuth.getCurrentUser();
							DatabaseReference ref;

							if (mUser != null) {
								if (isDJ) {
									ref = FirebaseDatabase.getInstance().getReference().child("dj_users").child(mUser.getUid());
									ref.child("genre").setValue(genre);
									ref.child("isLive").setValue(false);
								} else {
									ref = FirebaseDatabase.getInstance().getReference().child("regular_users").child(mUser.getUid());
								}

								uploadAndDownload(mUser.getUid(), isDJ, image, context, mListenerDismiss, isEditProfile);
								ref.child("email").setValue(email);
								ref.child("fullname").setValue(fullName);
							}

						} else {
							try {
								throw Objects.requireNonNull(task.getException());
							} catch (FirebaseAuthWeakPasswordException weakPass) {
								Toast.makeText(activity, "Your password is too weak",
										Toast.LENGTH_SHORT).show();
							} catch (FirebaseAuthInvalidCredentialsException malformedEmail) {
								Toast.makeText(activity, "Your email is malformed",
										Toast.LENGTH_SHORT).show();
							} catch (FirebaseAuthUserCollisionException existEmail) {
								Toast.makeText(activity, "This is email was already registered",
										Toast.LENGTH_SHORT).show();
							} catch (Exception e) {
								e.printStackTrace();
								Toast.makeText(activity, "Error occurred. Please try again",
										Toast.LENGTH_SHORT).show();
							}
						}
					}
				});
		}
	}

	// Function to get gallery for user to choose image from
	public static void startImageActivity(Activity activity, int REQUEST_IMAGE) {
			Intent imageIntent = new Intent(Intent.ACTION_PICK);
			imageIntent.setType("image/*");
			activity.startActivityForResult(imageIntent, REQUEST_IMAGE);
	}

	// Function to hide/show listview and hide/show empty state
	public static void hideOrShowList(ListView listView, ImageView imageView, TextView textView, boolean isHidden) {
		if (isHidden) {
			listView.setVisibility(View.INVISIBLE);
			imageView.setVisibility(View.VISIBLE);
			textView.setVisibility(View.VISIBLE);
		} else {
			listView.setVisibility(View.VISIBLE);
			imageView.setVisibility(View.INVISIBLE);
			textView.setVisibility(View.INVISIBLE);
		}
	}

	// Function to get image from Firebase based on UID
	public static void getImageFromFirebase(String uid, Resources resources, final ImageView imageView) {
		final Bitmap[] defaultImage = {BitmapFactory.decodeResource(resources, R.drawable.profile_image_default)};
		final StorageReference stoRef = FirebaseStorage.getInstance().getReference().child(uid);
		stoRef.getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
			@Override
			public void onSuccess(byte[] bytes) {
				defaultImage[0] = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
			}
		}).addOnCompleteListener(new OnCompleteListener<byte[]>() {
			@Override
			public void onComplete(@NonNull Task<byte[]> task) {
				imageView.setImageBitmap(MethodHelpers.getRoundedCornerBitmap(defaultImage[0]));
			}
		});
	}

	// FUnction to get bitmap from Firebase
	public static Bitmap getBitmapFromFirebase(String uid, Resources resources) {
		final Bitmap[] defaultImage = {BitmapFactory.decodeResource(resources, R.drawable.profile_image_default)};
		final StorageReference stoRef = FirebaseStorage.getInstance().getReference().child(uid);
		stoRef.getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
			@Override
			public void onSuccess(byte[] bytes) {
				defaultImage[0] = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
			}
		});

		return defaultImage[0];
	}

	// Function to create alert dialog for gallery and camera
	public static void createGalleryCameraAlert(final Activity activity, final CallCamera mListenerCam,
												final int REQUEST_IMAGE) {
		AlertDialog.Builder building = new AlertDialog.Builder(Objects.requireNonNull(activity));
		building.setTitle("Change Profile Image");
		building.setMessage("Choose one of the options below");

		building.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (mListenerCam != null) {
					mListenerCam.callCamera();
				}
			}
		});

		building.setNeutralButton("Cancel", null);

		building.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (ContextCompat.checkSelfPermission(activity,
						Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
					MethodHelpers.startImageActivity(activity, REQUEST_IMAGE);
				} else {
					ActivityCompat.requestPermissions(activity, new String[]
							{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_IMAGE);
				}
			}
		});
		building.create().show();
	}

	// Function to scale down the image
	public static Bitmap scaleBitmap(Bitmap image) {
		final int maxSize = 200;
		int outWidth;
		int outHeight;
		int currentBitmapHeight = image.getHeight();
		int currentBitmapWidth = image.getWidth();
		if (currentBitmapWidth > currentBitmapHeight) {
			outWidth = maxSize;
			outHeight = (currentBitmapHeight * maxSize) / currentBitmapWidth;
		} else {
			outHeight = maxSize;
			outWidth = (currentBitmapWidth * maxSize) / currentBitmapHeight;
		}

		return Bitmap.createScaledBitmap(image, outWidth, outHeight, false);
	}

	// Function to check if the editTexts are empty or all spaces
	public static String checkEditText(String email, String... strings) {
		String message = "";
		for (String word: strings) {
			if (word.replaceFirst(" ", "").isEmpty()) {
				message = "Please fill in all fields";
				break;
			}
		}

		if (email != null && !checkEmail(email)) {
			message = "Your email is not in the right format. Please try again";
		}

		return message;
	}

	// Function to check if email is the right format
	public static boolean checkEmail(String email) {
		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	// Function to upload image to Firebase and get the download url
	public static void uploadAndDownload(final String uid, final boolean isDJ, final Bitmap image, final Context context,
										 final DismissActivity mListener, final boolean isEditProfile) {
		Bitmap scaledImage = scaleBitmap(image);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();

		scaledImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		byte[] bitmapData = stream.toByteArray();


		// Create file metadata including the content type
		StorageMetadata metadata = new StorageMetadata.Builder()
				.setContentType("image/jpg")
				.build();

		final StorageReference storageRef = FirebaseStorage.getInstance().getReference().child(uid);
		// Upload the file and metadata
		storageRef.child(uid).putBytes(bitmapData, metadata).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
			@Override
			public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
				if (isEditProfile) {
					Toast.makeText(context, "Saved changes successfully", Toast.LENGTH_SHORT).show();
					if (mListener != null) {
						mListener.dismissActivity();
					}
				} else {
					storageRef.child(uid).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
						@Override
						public void onSuccess(Uri uri) {
							DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();
							if (isDJ) {
								mRef.child("dj_users").child(uid).child("image").setValue(uri.toString());
							} else {
								mRef.child("regular_users").child(uid).child("image").setValue(uri.toString());
							}
						}
					});
				}
			}
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				Toast.makeText(context, "Error occurred. Please try again.", Toast.LENGTH_SHORT).show();
			}
		});
	}
}
