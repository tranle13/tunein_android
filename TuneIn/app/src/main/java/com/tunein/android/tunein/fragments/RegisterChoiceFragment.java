package com.tunein.android.tunein.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.interfaces.ChooseRegistration;

public class RegisterChoiceFragment extends Fragment {

	// Variables
	private ChooseRegistration mListener;

	// Function to create new instance for fragment
	public static RegisterChoiceFragment newInstance() {

		Bundle args = new Bundle();

		RegisterChoiceFragment fragment = new RegisterChoiceFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if (context instanceof ChooseRegistration) {
			mListener = (ChooseRegistration)context;
		}
	}

	// Return fragment view
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_register_choice, container, false);
	}

	// Implement code for the whole fragment
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getContext() != null) {
			final Button button_Yes = getView().findViewById(R.id.button_Yes);
			final Button button_No = getView().findViewById(R.id.button_No);

			button_Yes.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mListener != null) {
						mListener.chooseRegistration(true);
					}
				}
			});

			button_No.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mListener != null) {
						mListener.chooseRegistration(false);
					}
				}
			});
		}
	}
}
