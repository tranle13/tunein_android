package com.tunein.android.tunein.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.helpers.MethodHelpers;

public class RegularLiveDJ extends Fragment {

	private static String currentLiveDJ;
	private FirebaseAuth mAuth;
	private DatabaseReference mRef;
	public static final String TAG = "RegularLiveDJ";
	private String djName;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAuth = FirebaseAuth.getInstance();
		mRef = FirebaseDatabase.getInstance().getReference();
	}

	// Create new instance of the fragment
	public static RegularLiveDJ newInstance(String uid) {

		Bundle args = new Bundle();

		currentLiveDJ = uid;
		RegularLiveDJ fragment = new RegularLiveDJ();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_regular_live_dj, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && mAuth.getUid() != null && getContext() != null) {
			final ImageView imageView_DJImage = getView().findViewById(R.id.imageView_RegularDJImage);
			final TextView textView_DJName = getView().findViewById(R.id.textView_DJName);
			final TextView textView_DJEmail = getView().findViewById(R.id.textView_DJEmail);
			final Button button_Follow = getView().findViewById(R.id.button_Follow);
			final Button button_SetAsLiveDJ = getView().findViewById(R.id.button_SetLiveDJ);

			mRef.child("dj_users").child(currentLiveDJ).addListenerForSingleValueEvent(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					String email = (String) dataSnapshot.child("email").getValue();
					djName = (String) dataSnapshot.child("fullname").getValue();
					String image = (String) dataSnapshot.child("image").getValue();

					if (email != null && djName != null) {
						textView_DJEmail.setText(email);
						textView_DJName.setText(djName);
						if (getContext() != null) {
							Glide.with(getContext()).load(image).into(imageView_DJImage);
						}
					}

					if (dataSnapshot.child("followers").child(mAuth.getUid()).getValue() != null) {
						button_Follow.setText(R.string.button_Unfollow);
					} else {
						button_Follow.setText(R.string.button_Follow);
					}

					mRef.child("regular_users").child(mAuth.getUid()).child("currentLiveDJ").addValueEventListener(new ValueEventListener() {
						@Override
						public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
							String setLiveDJ = (String)dataSnapshot.getValue();
							if (setLiveDJ != null && setLiveDJ.equals(currentLiveDJ)){
								button_SetAsLiveDJ.setText(R.string.button_RemoveCurrentDJ);
							} else {
								button_SetAsLiveDJ.setText(R.string.button_SetCurrentDJ);
							}
						}

						@Override
						public void onCancelled(@NonNull DatabaseError databaseError) {
							Log.w(TAG, "onCancelled: ", databaseError.toException());
						}
					});
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) { }
			});

			// Code to let current regular user follows the live DJ
			button_Follow.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (button_Follow.getText().toString().toLowerCase().equals("follow")) {
						mRef.child("regular_users").child(mAuth.getUid()).child("followed_djs").child(currentLiveDJ).setValue(djName);
						mRef.child("dj_users").child(currentLiveDJ).child("followers").child(mAuth.getUid()).setValue("following");
						button_Follow.setText(R.string.button_Unfollow);
					} else {
						mRef.child("regular_users").child(mAuth.getUid()).child("followed_djs").child(currentLiveDJ).removeValue();
						mRef.child("dj_users").child(currentLiveDJ).child("followers").child(mAuth.getUid()).removeValue();
						button_Follow.setText(R.string.button_Follow);
					}
				}
			});

			// Code to assign the chosen live DJ to the current regular user
			button_SetAsLiveDJ.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					if (button_SetAsLiveDJ.getText().toString().toLowerCase().equals("set as live dj")) {
						mRef.child("regular_users").child(mAuth.getUid()).child("currentLiveDJ").setValue(currentLiveDJ);
						button_SetAsLiveDJ.setText(R.string.button_RemoveCurrentDJ);
					} else {
						mRef.child("regular_users").child(mAuth.getUid()).child("currentLiveDJ").removeValue();
						button_SetAsLiveDJ.setText(R.string.button_SetCurrentDJ);
					}
				}
			});
		}
	}
}
