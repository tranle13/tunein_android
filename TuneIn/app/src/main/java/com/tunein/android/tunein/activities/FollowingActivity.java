package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.FollowFragment;
import com.tunein.android.tunein.interfaces.CallRespectiveActivity;

public class FollowingActivity extends AppCompatActivity implements CallRespectiveActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_following);

		Intent startIntent = getIntent();
		if (startIntent != null) {

			boolean isDJ = startIntent.getBooleanExtra("isDJ", false);

			getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_6,
					FollowFragment.newInstance(isDJ)).commit();
		}
	}

	@Override
	public void callRespectiveActivity(int which, String uid) {
		if (which == FollowFragment.listViewID) {
			Intent startIntent = new Intent(this, DJInfoActivity.class);
			startIntent.putExtra("uid", uid);
			startActivity(startIntent);
		}
	}
}
