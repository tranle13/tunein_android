package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.UserProfileFragment;

public class UserProfileActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile);

		Intent startIntent = getIntent();

		if (startIntent != null && startIntent.getBooleanExtra("isDJ", false)) {
			getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_7,
					UserProfileFragment.newInstance(startIntent.getBooleanExtra("isDJ", false))).commit();
		}
	}
}
