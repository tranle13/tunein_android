package com.tunein.android.tunein.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.interfaces.DismissActivity;

import java.util.ArrayList;
import java.util.Objects;

public class DJViewGenresFragment extends ListFragment {

	private FirebaseAuth mAuth;
	private DatabaseReference mRef;
	private static final String TAG = "DJViewGenresFragment";
	private DismissActivity mListener;

	public static DJViewGenresFragment newInstance() {
		return new DJViewGenresFragment();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof DismissActivity) {
			mListener = (DismissActivity)context;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_profile_dj_view_genres, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAuth = FirebaseAuth.getInstance();
		mRef = FirebaseDatabase.getInstance().getReference().child("dj_users");

		if (getView() != null && mAuth.getUid() != null && getContext() != null) {
			mRef.child(mAuth.getUid()).child("genres").addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					if (dataSnapshot.getValue() != null) {
						String[] allGenres = dataSnapshot.getValue().toString().split(",");
						ArrayList<String> allMusic = new ArrayList<>();
						for (String genre: allGenres) {
							allMusic.add(genre.trim());
						}

						ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),
								android.R.layout.simple_list_item_1, allMusic);
						setListAdapter(adapter);
					}
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.w(TAG, "onCancelled: ", databaseError.toException());
				}
			});
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_edit, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_btn_edit) {
			if (mListener != null) {
				mListener.dismissActivity();
			}
		}

		return super.onOptionsItemSelected(item);
	}
}
