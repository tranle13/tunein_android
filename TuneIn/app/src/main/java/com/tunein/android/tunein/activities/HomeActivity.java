package com.tunein.android.tunein.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.DataVisChoiceFragment;
import com.tunein.android.tunein.fragments.FollowFragment;
import com.tunein.android.tunein.fragments.HomeFragment;
import com.tunein.android.tunein.fragments.ProfileFragment;
import com.tunein.android.tunein.fragments.QrCodeFragment;
import com.tunein.android.tunein.fragments.SongFragment;
import com.tunein.android.tunein.fragments.TipFragment;
import com.tunein.android.tunein.interfaces.CallRespectiveActivity;
import com.tunein.android.tunein.interfaces.SignOut;

public class HomeActivity extends AppCompatActivity implements CallRespectiveActivity, SignOut {

	// Variables
	private String currentUid = "";
	private Boolean isDJ = false;
	public static final String TAG = "HomeActivity";
	public static final String EXTRA_RESULT_RECEIVER = "com.tunein.android.tunein.activities.EXTRA_RESULT_RECEIVER";
	public static final String EXTRA_SEARCH_TERM = "EXTRA_SEARCH_TERM";

	private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
			= new BottomNavigationView.OnNavigationItemSelectedListener() {

		@Override
		public boolean onNavigationItemSelected(@NonNull MenuItem item) {
			if (getSupportActionBar() != null) {
				switch (item.getItemId()) {
					case R.id.tab_btn_home:
						getSupportActionBar().setTitle("Home");
						getSupportFragmentManager().beginTransaction().replace(R.id.container, HomeFragment.newInstance(isDJ)).commit();
						return true;
					case R.id.tab_btn_song:
						getSupportActionBar().setTitle("Song Request");
						getSupportFragmentManager().beginTransaction().replace(R.id.container, SongFragment.newInstance(isDJ)).commit();
						return true;
					case R.id.tab_btn_qr:
						getSupportActionBar().setTitle("QR Code");
						getSupportFragmentManager().beginTransaction().replace(R.id.container, QrCodeFragment.newInstance(isDJ)).commit();
						return true;
					case R.id.tab_btn_tip:
						getSupportActionBar().setTitle("Tip");
						getSupportFragmentManager().beginTransaction().replace(R.id.container, TipFragment.newInstance(isDJ)).commit();
						return true;
					case R.id.tab_btn_profile:
						getSupportActionBar().setTitle("Profile");
						getSupportFragmentManager().beginTransaction().replace(R.id.container, ProfileFragment.newInstance(isDJ)).commit();
						return true;
				}
			}
			return false;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle("Home");
		}

		FirebaseAuth mAuth = FirebaseAuth.getInstance();
		if (mAuth.getCurrentUser() != null) {
			currentUid = mAuth.getCurrentUser().getUid();
			DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("dj_users");
			ref.addListenerForSingleValueEvent(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					if (dataSnapshot.hasChild(currentUid)) {
						isDJ = true;
					}

					getSupportFragmentManager().beginTransaction().replace(R.id.container, HomeFragment.newInstance(isDJ)).commit();

				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.i(TAG, "onCancelled: ");
				}
			});
		}

		BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
		navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
	}

	// Function to see if which fragment it should add
	@Override
	public void callRespectiveActivity(int which, String uid) {
		Class destination;
		switch (which) {
			case R.id.button_EditProfile:
				destination = EditProfileActivity.class;
				break;
			case R.id.button_Notifications:
				destination = NotificationsActivity.class;
				break;
			case R.id.button_Follow:
				destination = FollowingActivity.class;
				break;
			case R.id.button_TipChart:
				destination = DataVisChoiceActivity.class;
				break;
			case R.id.button_Genres:
				destination = ViewGenreActivity.class;
				break;
			case R.id.menu_btn_search:
				destination = SearchSongActivity.class;
				break;
			case R.id.listview_CurrentLiveDJs:
				destination = LiveDJActivity.class;
				break;
			case R.id.scanner:
				destination = DJInfoActivity.class;
				break;
			default:
				destination = EditProfileActivity.class;
				Log.i(TAG, "Couldn't find any UI components with this id");
				break;
		}

		Intent startIntent = new Intent(this, destination);
		startIntent.putExtra("isDJ", isDJ);
		startIntent.putExtra("uid", uid);
		startActivity(startIntent);
	}

	// Interface to dismiss all activities before going back to the log in screen when user chooses to sign out
	@Override
	public void signOut() {
		Intent finishIntent = new Intent(this, MainActivity.class);
		ActivityCompat.finishAffinity(this);
		startActivity(finishIntent);
	}
}
