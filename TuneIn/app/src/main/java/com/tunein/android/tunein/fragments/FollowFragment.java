package com.tunein.android.tunein.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tunein.android.tunein.R;
import com.tunein.android.tunein.adapters.LiveDJAdapter;
import com.tunein.android.tunein.custom_objects.Person;
import com.tunein.android.tunein.helpers.MethodHelpers;
import com.tunein.android.tunein.interfaces.CallRespectiveActivity;

import java.util.ArrayList;

public class FollowFragment extends ListFragment {

	// Member variables
	private static boolean isDJ;
	private FirebaseAuth mAuth;
	private DatabaseReference mRef;
	private ArrayList<Person> followedDJs = new ArrayList<>();
	private ArrayList<Person> following = new ArrayList<>();
	private CallRespectiveActivity mListener;
	public static final int listViewID = 0x11011;

	// Initializer
	public static FollowFragment newInstance(boolean isDJOrNot) {

		Bundle args = new Bundle();

		isDJ = isDJOrNot;

		FollowFragment fragment = new FollowFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if (context instanceof CallRespectiveActivity) {
			mListener = (CallRespectiveActivity)context;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_follow, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAuth = FirebaseAuth.getInstance();
		mRef = FirebaseDatabase.getInstance().getReference();
		String childName;
		String followChildName;

		if (getView() != null && mAuth.getUid() != null && getContext() != null) {
			if (isDJ) {
				childName = "dj_users";
				followChildName = "followers";
			} else {
				childName = "regular_users";
				followChildName = "followed_djs";
			}

			mRef.child(childName).child(mAuth.getUid()).child(followChildName).addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					if (!isDJ) {
						for (DataSnapshot snapshot_1 : dataSnapshot.getChildren()) {
							String uid = snapshot_1.getKey();
							String name = (String)snapshot_1.getValue();
							String image = (String)snapshot_1.child("image").getValue();

							followedDJs.add(new Person(name, uid, "", isDJ, image));
						}

						LiveDJAdapter adapter = new LiveDJAdapter(followedDJs, getContext(), null);
						setListAdapter(adapter);

					} else {
						final ArrayList<String> uid = new ArrayList<>();
						for (final DataSnapshot snapshot_2: dataSnapshot.getChildren()) {
							uid.add(snapshot_2.getKey());
						}

						mRef.child("regular_users").addListenerForSingleValueEvent(new ValueEventListener() {
							@Override
							public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
								for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
									if (uid.contains(snapshot.getKey())) {
										String name = (String)snapshot.child("fullname").getValue();
										String email = (String)snapshot.child("email").getValue();
										String image = (String)snapshot.child("image").getValue();
										following.add(new Person(name, snapshot.getKey(), email, isDJ, image));
									}
								}

								LiveDJAdapter adapter = new LiveDJAdapter(following, getContext(), null);
								setListAdapter(adapter);
							}

							@Override
							public void onCancelled(@NonNull DatabaseError databaseError) { }
						});
					}
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) { }
			});
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		if (mListener != null) {
			mListener.callRespectiveActivity(listViewID, followedDJs.get(position).getUid());
		}
	}
}
