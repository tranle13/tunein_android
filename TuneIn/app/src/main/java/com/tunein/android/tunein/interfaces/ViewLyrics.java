package com.tunein.android.tunein.interfaces;

import com.tunein.android.tunein.custom_objects.Track;

public interface ViewLyrics {
	void viewLyrics(Track track);
}
