package com.tunein.android.tunein.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tunein.android.tunein.fragments.NotificationFinancialFragment;
import com.tunein.android.tunein.fragments.NotificationOtherFragment;

public class NotificationPageAdapter extends FragmentStatePagerAdapter {

	private boolean isDJ;

	public NotificationPageAdapter(FragmentManager fm, boolean _isDJ) {
		super(fm);
		isDJ = _isDJ;
	}

	@Override
	public Fragment getItem(int i) {
		switch (i) {
			case 0:
				return NotificationFinancialFragment.newInstance(isDJ);
			default:
				return NotificationOtherFragment.newInstance(isDJ);
		}
	}

	@Override
	public int getCount() {
		return 2;
	}
}
