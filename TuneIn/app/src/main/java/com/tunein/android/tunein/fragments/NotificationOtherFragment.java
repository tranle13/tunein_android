package com.tunein.android.tunein.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tunein.android.tunein.R;

import java.util.ArrayList;
import java.util.Objects;

public class NotificationOtherFragment extends ListFragment {

	// Variables
	private static boolean isDJ;
	private FirebaseAuth mAuth;
	private DatabaseReference mRef;
	private static final String TAG = "NotificationOtherFrag";
	private ArrayList<String> notifications = new ArrayList<>();

	// Initializer
	public static NotificationOtherFragment newInstance(boolean _isDJ) {
		isDJ = _isDJ;
		return new NotificationOtherFragment();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_notifications_others, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAuth = FirebaseAuth.getInstance();
		mRef = FirebaseDatabase.getInstance().getReference();

		if (getView() != null && mAuth.getUid() != null && getContext() != null) {

			String childname = "regular_users";
			String notiChildname = "live_djs";
			if (isDJ) {
				childname = "dj_users";
				notiChildname = "new_followers";
			}

			mRef.child(childname).child(mAuth.getUid()).child("notifications").child(notiChildname).addValueEventListener(new ValueEventListener() {
				@Override
				public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
					for (DataSnapshot subSnap : dataSnapshot.getChildren()) {
						if (subSnap.getValue() != null) {
							notifications.add(subSnap.getValue().toString());
						}
					}

					ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),
							android.R.layout.simple_list_item_1, notifications);
					setListAdapter(adapter);
				}

				@Override
				public void onCancelled(@NonNull DatabaseError databaseError) {
					Log.w(TAG, "onCancelled: ", databaseError.toException());
				}
			});
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_delete, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_btn_delete && mAuth.getUid() != null) {
			String childname = "regular_users";
			String notiChildname = "live_djs";
			if (isDJ) {
				childname = "dj_users";
				notiChildname = "new_followers";
			}
			mRef.child(childname).child(mAuth.getUid()).child("notifications").child(notiChildname).removeValue();
		}
		return super.onOptionsItemSelected(item);
	}
}
