package com.tunein.android.tunein.services;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import com.tunein.android.tunein.activities.HomeActivity;
import com.tunein.android.tunein.custom_objects.Track;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

public class TrackIntentService extends IntentService {

	// Member variable
	private static final String name = "TrackIntentService";

	public TrackIntentService() {
		super("TrackIntentService");
	}

	// Function to download the track information from API
	@Override
	protected void onHandleIntent(@Nullable Intent intent) {
		ResultReceiver receiver = null;
		String searchTerm = "";

		// Get ResultReceiver from intent
		try {
			if (intent != null) {
				receiver = intent.getParcelableExtra(HomeActivity.EXTRA_RESULT_RECEIVER);
				searchTerm = intent.getStringExtra(HomeActivity.EXTRA_SEARCH_TERM);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		URL url = null;
		HttpURLConnection connection = null;
		InputStream stream = null;

		ArrayList<Track> spotify = new ArrayList<>();

		// Open connection
		try {
			String webAddress = "https://ws.audioscrobbler.com/2.0/?method=track.search" +
					"&track=" + searchTerm + "&api_key=e1304e5bec6128eb33251f11404b0716&format=json";
			url = new URL(webAddress);
			connection = (HttpURLConnection)url.openConnection();
			connection.connect();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Open stream and close after done getting data
		try {
			stream = Objects.requireNonNull(connection).getInputStream();
			String result = IOUtils.toString(stream, "UTF-8");

			JSONObject outerObj = new JSONObject(result);
			JSONObject innerObj = outerObj.getJSONObject("results");
			JSONObject subObj = innerObj.getJSONObject("trackmatches");
			JSONArray tracks = subObj.getJSONArray("track");

			for (int i = 0; i < tracks.length(); i++) {
				JSONObject object = tracks.getJSONObject(i);
				String songName = object.getString("name");
				String songArtist = object.getString("artist");
				JSONArray images = object.getJSONArray("image");

				for (int j = 0; j < images.length(); j++) {
					if (j == 2) {
						JSONObject image = images.getJSONObject(j);
						String cover = image.getString("#text");

						spotify.add(new Track(songName, songArtist, cover));
						break;
					}

				}
			}

			if (receiver != null) {
				Bundle data = new Bundle();
				data.putSerializable("RESULT", spotify);
				receiver.send(Activity.RESULT_OK, data);
			}

			connection.disconnect();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
