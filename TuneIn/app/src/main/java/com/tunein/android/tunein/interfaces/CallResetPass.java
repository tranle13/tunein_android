package com.tunein.android.tunein.interfaces;

public interface CallResetPass {
	void callResetPass(String email);
}
