package com.tunein.android.tunein.interfaces;

import com.tunein.android.tunein.custom_objects.Track;

public interface RequestSong {
	void requestSong(Track chosenTrack);
}
