package com.tunein.android.tunein.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.activities.HomeActivity;
import com.tunein.android.tunein.custom_objects.Track;
import com.tunein.android.tunein.helpers.MethodHelpers;
import com.tunein.android.tunein.services.LyricsIntentService;

import java.util.Objects;

public class LyricsFragment extends Fragment {

	// Member variables
	private static Track track;
	private ProgressBar progressBar;
	private TextView textView_Lyrics;
	private ConstraintLayout emptyState;
	public static final String EXTRA_TRACK = "EXTRA_TRACK";

	public static LyricsFragment newInstance(Track chosenTrack) {

		Bundle args = new Bundle();

		track = chosenTrack;

		LyricsFragment fragment = new LyricsFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_lyrics, container, false);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (getView() != null && getContext() != null && getActivity() != null) {
			textView_Lyrics = getView().findViewById(R.id.textView_Lyrics);
			progressBar = getView().findViewById(R.id.progress);
			emptyState = getView().findViewById(R.id.empty_state);

			textView_Lyrics.setMovementMethod(new ScrollingMovementMethod());

			if (MethodHelpers.isConnected(getContext())) {
				Intent startIntent = new Intent(getContext(), LyricsIntentService.class);
				startIntent.putExtra(HomeActivity.EXTRA_RESULT_RECEIVER, new LyricsResultReceiver());
				startIntent.putExtra(EXTRA_TRACK, track);
				Objects.requireNonNull(getActivity()).startService(startIntent);
				progressBar.setVisibility(View.VISIBLE);
			} else {
				Toast.makeText(getContext(), "Please connect to the internet and try again", Toast.LENGTH_SHORT).show();
			}
		}
	}

	// Create handler and result receiver to communicate with main thread
	private final Handler mHandler = new Handler();
	public class LyricsResultReceiver extends ResultReceiver {

		// Constructor
		LyricsResultReceiver() {
			super(mHandler);
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			super.onReceiveResult(resultCode, resultData);
			if (resultData != null) {
				progressBar.setVisibility(View.GONE);
				String lyrics = resultData.getString("lyrics");

				if (lyrics != null) {
					if (lyrics.length() > 0) {
						textView_Lyrics.setText(lyrics);
						emptyState.setVisibility(View.GONE);
					} else {
						emptyState.setVisibility(View.VISIBLE);
					}
				} else {
					emptyState.setVisibility(View.VISIBLE);
				}
			}
		}
	}
}
