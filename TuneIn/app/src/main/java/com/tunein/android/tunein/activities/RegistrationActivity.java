package com.tunein.android.tunein.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.tunein.android.tunein.R;
import com.tunein.android.tunein.fragments.DJRegisterFragment;
import com.tunein.android.tunein.fragments.RegularRegisterFragment;
import com.tunein.android.tunein.helpers.MethodHelpers;
import com.tunein.android.tunein.interfaces.CallCamera;
import com.tunein.android.tunein.interfaces.ChangeActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class RegistrationActivity extends AppCompatActivity implements ChangeActivity, CallCamera {

	// Member variables
	private static final int REQUEST_CAMERA = 0x00010;
	private static final String AUTHORITY = "com.tunein.android.imageprovider";
	private static final String IMAGE_FOLDER = "capture_images";
	private static final String TAG = "RegistrationActivity";
	private File storage;
	private File imageFile;
	private int genreRequestCode = 0x1101;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);

		Intent startIntent = getIntent();

		storage = getExternalFilesDir(IMAGE_FOLDER);

		if (startIntent != null) {
			final boolean isDJ = startIntent.getBooleanExtra("isDJ", false);

			if (isDJ) {
				getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_3, DJRegisterFragment.newInstance()).commit();
			} else {
				getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer_3, RegularRegisterFragment.newInstance()).commit();
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && data != null) {
			if (requestCode == genreRequestCode) {
				DJRegisterFragment.genre = data.getStringExtra("genre");
			} else if (requestCode == REQUEST_CAMERA) {
				DJRegisterFragment.capturedImage = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
			} else if (requestCode == DJRegisterFragment.REQUEST_IMAGE) {
				final Uri imageUri = data.getData();
				try {
					DJRegisterFragment.capturedImage = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void changeActivity(Boolean isHome) {
		if (isHome) {
			Intent toHome = new Intent(this, HomeActivity.class);
			toHome.putExtra("isDJ", isHome);
			startActivity(toHome);
		} else {
			Intent toGenre = new Intent(this, GenreActivity.class);
			startActivityForResult(toGenre, genreRequestCode);
		}
	}

	// Function to get Uri for file
	private Uri getOutputUri() {
		imageFile = getImageFile();

		if (imageFile == null) {
			return null;
		}

		return FileProvider.getUriForFile(this, AUTHORITY, imageFile.getAbsoluteFile());
	}

	// Function to create new file to store images
	private File getImageFile() {
		File protectedStorage = getExternalFilesDir(IMAGE_FOLDER);
		File imageFile = new File(protectedStorage, "profile_image");

		try {
			boolean canCreate = imageFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return imageFile;
	}

	@Override
	public void callCamera() {
		if (ContextCompat.checkSelfPermission(this,
				Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
			Intent photoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			photoIntent.putExtra(MediaStore.EXTRA_OUTPUT, getOutputUri());
			photoIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
			startActivityForResult(photoIntent, REQUEST_CAMERA);
		} else {
			ActivityCompat.requestPermissions(this, new String[]
					{Manifest.permission.CAMERA}, REQUEST_CAMERA);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			if (requestCode == REQUEST_CAMERA) {
				Intent photoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				photoIntent.putExtra(MediaStore.EXTRA_OUTPUT, getOutputUri());
				photoIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
				startActivityForResult(photoIntent, REQUEST_CAMERA);
			} else {
				MethodHelpers.startImageActivity(this, DJRegisterFragment.REQUEST_IMAGE);
			}
		}
	}
}
